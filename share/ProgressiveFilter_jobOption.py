from AthenaCommon.Constants import *
from AthenaCommon.AppMgr import theApp
from AthenaCommon.AppMgr import ServiceMgr
import AthenaPoolCnvSvc.ReadAthenaPool

ServiceMgr.AuditorSvc.Auditors += ["ChronoAuditor"]

AthenaPoolCnvSvc = Service("AthenaPoolCnvSvc")
AthenaPoolCnvSvc.UseDetailChronoStat = TRUE

from PartPropSvc.PartPropSvcConf import PartPropSvc

include("ParticleBuilderOptions/McAOD_PoolCnv_jobOptions.py")
include("EventAthenaPool/EventAthenaPool_joboptions.py")

# build GeoModel
DetDescrVersion = 'ATLAS-P2-ITK-17-04-00'
from AthenaCommon.DetFlags import DetFlags
DetFlags.ID_setOn()
DetFlags.TRT_setOff()
DetFlags.Calo_setOn()
DetFlags.HGTD_setOn()
DetFlags.Muon_setOn()
DetFlags.Truth_setOn()

from AthenaCommon.GlobalFlags import globalflags
globalflags.DetDescrVersion = DetDescrVersion

from TrkDetDescrSvc.TrkDetDescrJobProperties import TrkDetFlags
TrkDetFlags.InDetBuildingOutputLevel = VERBOSE

include("InDetSLHC_Example/preInclude.SLHC.py")
SLHC_Flags.LayoutOption = 'InclinedAlternative'
SLHC_Flags.doGMX.set_Value_and_Lock(True)

from AtlasGeoModel import SetGeometryVersion 
from AtlasGeoModel import GeoModelInit 

from GeoModelSvc.GeoModelSvcConf import GeoModelSvc
GeoModelSvc = GeoModelSvc()
ServiceMgr += GeoModelSvc
GeoModelSvc.AtlasVersion = DetDescrVersion

include("InDetSLHC_Example/preInclude.SLHC_Setup_InclBrl_4.py")
include("InDetSLHC_Example/preInclude.SLHC_Setup_Strip_GMX.py")
include('InDetSLHC_Example/SLHC_Setup_Reco_TrackingGeometry_GMX.py')

#----------------------------
# Input Dataset
#----------------------------
import os
from glob import glob
from AthenaCommon.AthenaCommonFlags import athenaCommonFlags

# #  
# # hmmm, the xrd python module does't seem to be available in athena on slc6? 
# #  booo!  
# def globFromXRD(dir, fileString):
#     from XRootD import client
#     from XRootD.client.flags import DirListFlags, OpenFlags, MkDirFlags, QueryCode
#     import re
#     pattern = re.compile(fileString)
#     myclient = client.FileSystem('root://eosatlas.cern.ch:1094') 

#     status, listing = myclient.dirlist(dir, DirListFlags.STAT)
#     found = []
#     for entry in listing:
#       if pattern.match(entry.name):
#             print "root://eosatlas.cern.ch/"+dir+"/"+entry.name 
#             found += ["root://eosatlas.cern.ch/"+dir+"/"+entry.name]
#     return found

athenaCommonFlags.FilesInput = glob( "/eos/atlas/atlastier0/tzero/scratch/physmon/dev_noemi/ESDS_AnalogClustering_NEWDIGI_Feb28/single_muon_Pt45/ESD.Step3ITk.anal.HGTD.single_muon_Pt45.3.pool.root" )
# Max has no /eos in his docker thing, so Max needs a horrible workaround here. Max is sorry. 
# athenaCommonFlags.FilesInput = globFromXRD("/eos/atlas/atlastier0/tzero/scratch/physmon/dev_noemi/ESDS_AnalogClustering_NEWDIGI_Feb28/single_muon_Pt45_etaFlatnp23_32","ESD.Step3ITk.anal.HGTD.single_muon_Pt45_etaFlatnp23_32.*.pool.root")
ServiceMgr.EventSelector.InputCollections = athenaCommonFlags.FilesInput()
#theApp.EvtMax= 50000
#ServiceMgr.EventSelector.SkipEvents = 280
#theApp.EvtMax = 20
theApp.EvtMax = -1

#----------------------------
# Message Service
#----------------------------
# set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL)
ServiceMgr.MessageSvc.OutputLevel = INFO
ServiceMgr.MessageSvc.defaultLimit = 9999999
ServiceMgr.MessageSvc.setError +=  [ "HepMcParticleLink"]

#----------------------------
# Algorithms
#----------------------------
#topSequence.AthenaCommonFlags.HistOutputs = ["TrackTimingExtensionAlg:myfile.root"]
#svcMgr += CfgMgr.THistSvc()
#svcMgr.THistSvc.Output += ["TrackTimingExtensionAlg DATAFILE='myfile.root' OPT='RECREATE'"]

from AthenaCommon.AlgSequence import AlgSequence
topSequence = AlgSequence()
from TrkExTools.TrkExToolsConf import Trk__TrackTimingExtensionAlg
topSequence += Trk__TrackTimingExtensionAlg("TrackTimingExtensionAlg", OutputLevel=INFO)
topSequence += Trk__TrackTimingExtensionAlg("TrackTimingExtensionAlg")
# Shahzad Added
#topSequence.TrackTimingExtensionAlg.HistPath = 'TrackTimingExtensionAlg/histos'
svcMgr += CfgMgr.THistSvc()
svcMgr.THistSvc.Output += ["TrackTimingExtensionAlg DATAFILE='myfile.root' OPT='RECREATE'"]
#---------------------------
# Detector geometry
#---------------------------
from RecExConfig.AutoConfiguration import *
ConfigureFieldAndGeo()
include("RecExCond/AllDet_detDescr.py")

include("InDetSLHC_Example/postInclude.SLHC_Setup_InclBrl_4.py")


# analysis and output histos 

svcMgr += CfgMgr.THistSvc()
svcMgr.THistSvc.Output += ["HGTDTimeExtension DATAFILE='HGTDPlots.root' OPT='RECREATE'"]
#svcMgr.THistSvc.Output += ["HGTDTESD DATAFILE='HGTDHISTOS.root' OPT='RECREATE'"]

from TrkExTools.TrkExToolsConf import Trk__TrackTimingExtensionPlots
topSequence += Trk__TrackTimingExtensionPlots("TrackTimingExtensionPlots")

# output - shamelessly stolen from standalone tracking setup! 

print "========= NOW SETTING UP FOR OUTPUT ============"

from InDetRecExample.InDetJobProperties import InDetFlags

  # --- load setup
from AthenaPoolCnvSvc.WriteAthenaPool import AthenaPoolOutputStream
  # --- check dictionary
ServiceMgr.AthenaSealSvc.CheckDictionary = True
  # --- commit interval (test)
ServiceMgr.AthenaPoolCnvSvc.CommitInterval = 10

# MC truth information
truthList = [ 'xAOD::TruthEventContainer#TruthEvents',
            'xAOD::TruthEventAuxContainer#TruthEventsAux.',
            'xAOD::TruthEventContainer#TruthEvents_PU',
            'xAOD::TruthEventAuxContainer#TruthEvents_PUAux.',
            'xAOD::TruthEventContainer#SpclMCPU',
            'xAOD::TruthEventAuxContainer#SpclMCPUAux.',
            'xAOD::TruthPileupEventContainer#TruthPileupEvents',
            'xAOD::TruthPileupEventAuxContainer#TruthPileupEventsAux.',
            'xAOD::TruthParticleContainer#TruthParticles',
            'xAOD::TruthParticleAuxContainer#TruthParticlesAux.',
            'xAOD::TruthVertexContainer#TruthVertices',
            'xAOD::TruthVertexAuxContainer#TruthVerticesAux.',
            'PileUpEventInfo#OverlayEvent' ]

# --- create stream
StreamESD            = AthenaPoolOutputStream ( "StreamESD", "ESD.pool.root",asAlg=True) 
# --- save MC collections if truth turned on
StreamESD.ItemList += truthList
# ---- load list of objects
include ( "InDetRecExample/WriteInDetESD.py" )
StreamESD.ItemList  += InDetESDList
StreamESD.ItemList  += [ 'xAOD::EventInfo#EventInfo' , 'xAOD::EventAuxInfo#EventInfoAux.' ]
StreamESD.ForceRead = True # otherwise unread stuff is not copied
