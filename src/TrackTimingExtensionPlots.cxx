#include "TrkExTools/TrackTimingExtensionPlots.h"

#include "AtlasDetDescr/AtlasDetectorID.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GeoPrimitives/GeoPrimitives.h"
#include "InDetIdentifier/PixelID.h"
#include "InDetIdentifier/SCT_ID.h"
#include "InDetIdentifier/SiliconID.h"
#include "InDetRIO_OnTrack/PixelClusterOnTrack.h"
#include "InDetReadoutGeometry/SiDetectorElement.h"
#include "TrkDetDescrUtils/BinUtility.h"
#include "TrkDetDescrUtils/BinnedArray1D1D.h"
#include "TrkGeometry/PlaneLayer.h"
#include "TrkSurfaces/RectangleBounds.h"
#include "xAODTracking/TrackParticleContainer.h"
using namespace std; 
namespace Trk {
    typedef std::pair<SharedObject<const Surface>, Amg::Vector3D> SurfaceOrderPosition;
}

Trk::TrackTimingExtensionPlots::TrackTimingExtensionPlots(const std::string& name, ISvcLocator* pSvcLocator) :
    AthAlgorithm(name, pSvcLocator),
    m_deco_name("HGTD_Clusters"),
    m_TrackParticleContainer("InDetTrackParticles"),
    m_dec_extensionCandidate(nullptr),
    m_dec_hasValidExtension(nullptr),
    m_dec_nHGTDLayers(nullptr),
    m_dec_trueProdTime(nullptr),
    m_dec_meanTime(nullptr),
    m_dec_meanTimeChi2Weighted(nullptr),
    m_dec_rmsTime(nullptr),
    m_dec_RonLayer0(nullptr),
    m_dec_perLayer_hasCluster(nullptr),
    m_dec_perLayer_clusterChi2(nullptr),
    m_dec_perLayer_clusterR(nullptr),
    m_dec_perLayer_clusterRawTime(nullptr),
    m_dec_perLayer_clusterT0(nullptr),
    m_dec_perLayer_clusterDeltaT(nullptr),
    m_dec_perLayer_clusterX(nullptr),
    m_dec_perLayer_clusterY(nullptr),
    m_dec_perLayer_clusterZ(nullptr),
    m_histSvc("THistSvc",name)
     {
    declareProperty("DecorationNamePrefix", m_deco_name);
}

Trk::TrackTimingExtensionPlots::~TrackTimingExtensionPlots() {}

StatusCode Trk::TrackTimingExtensionPlots::initialize() {
    // Grab PixelID helper

    ATH_CHECK(m_histSvc.retrieve());

    // set up our decos
    m_dec_extensionCandidate = std::make_unique<SG::AuxElement::Accessor<char> >(m_deco_name + "_candidateForExtension");

    m_dec_hasValidExtension = std::make_unique<SG::AuxElement::Accessor<char> >(m_deco_name + "_hasValidExtension");

    m_dec_nHGTDLayers = std::make_unique<SG::AuxElement::Accessor<int> >(m_deco_name + "_nHGTDLayers");
    m_dec_trueProdTime = std::make_unique<SG::AuxElement::Accessor<float> >(m_deco_name + "_trueProductionTime");

    m_dec_meanTime = std::make_unique<SG::AuxElement::Accessor<float> >(m_deco_name + "_meanTimeArithmeticMean");
    m_dec_meanTimeChi2Weighted = std::make_unique<SG::AuxElement::Accessor<float> >(m_deco_name + "_meanTimeChi2Weighted");

    m_dec_rmsTime = std::make_unique<SG::AuxElement::Accessor<float> >(m_deco_name + "_rmsTime");

    m_dec_RonLayer0 = std::make_unique<SG::AuxElement::Accessor<float> >(m_deco_name + "_RonLayer0");

    m_dec_perLayer_hasCluster = std::make_unique<SG::AuxElement::Accessor<std::vector<bool> > >(m_deco_name + "_perLayer_hasCluster");

    m_dec_perLayer_clusterChi2 = std::make_unique<SG::AuxElement::Accessor<std::vector<float> > >(m_deco_name + "_perLayer_clusterChi2");

    m_dec_perLayer_clusterR = std::make_unique<SG::AuxElement::Accessor<std::vector<float> > >(m_deco_name + "_perLayer_clusterR");

    m_dec_perLayer_clusterRawTime =
        std::make_unique<SG::AuxElement::Accessor<std::vector<float> > >(m_deco_name + "_perLayer_clusterRawTime");

    m_dec_perLayer_clusterT0 = std::make_unique<SG::AuxElement::Accessor<std::vector<float> > >(m_deco_name + "_perLayer_clusterT0");
    m_dec_perLayer_clusterDeltaT =
        std::make_unique<SG::AuxElement::Accessor<std::vector<float> > >(m_deco_name + "_perLayer_clusterDeltaT");

    m_dec_perLayer_clusterX = std::make_unique<SG::AuxElement::Accessor<std::vector<float> > >(m_deco_name + "_perLayer_clusterX");
    m_dec_perLayer_clusterY = std::make_unique<SG::AuxElement::Accessor<std::vector<float> > >(m_deco_name + "_perLayer_clusterY");
    m_dec_perLayer_clusterZ = std::make_unique<SG::AuxElement::Accessor<std::vector<float> > >(m_deco_name + "_perLayer_clusterZ");

    // histo setup 

    nHits_raw = new TH1D("Raw_NHits",";nb. of clusters;a.u.",5,-0.5,4.5);
    eff_eta_atLeastOneA = new TEfficiency("Effi_vs_eta_atLeastOneHitASide", "Effi;#eta;eff",24,2.4,4.0);
    eff_eta_atLeastTwoA = new TEfficiency("Effi_vs_eta_atLeastTwoHitsASide", "Effi;#eta;eff",24,2.4,4.0);
    eff_eta_atLeastThreeA = new TEfficiency("Effi_vs_eta_atLeastThreeHitsASide", "Effi;#eta;eff",24,2.4,4.0);
    eff_eta_atLeastOneC = new TEfficiency("Effi_vs_eta_atLeastOneHitCSide", "Effi;#eta;eff",24,-4.0,-2.4);
    eff_eta_atLeastTwoC = new TEfficiency("Effi_vs_eta_atLeastTwoHitsCSide", "Effi;#eta;eff",24,-4.0,-2.4);
    eff_eta_atLeastThreeC = new TEfficiency("Effi_vs_eta_atLeastThreeHitsCSide", "Effi;#eta;eff",24,-4.0,-2.4);
    eff_phi_atLeastOne = new TEfficiency("Effi_vs_phi_atLeastOneHit", "Effi;#phi;eff",24,-3.15,3.15);
    eff_phi_atLeastTwo = new TEfficiency("Effi_vs_phi_atLeastTwoHits", "Effi;#phi;eff",24,-3.15,3.15);
    eff_phi_atLeastThree = new TEfficiency("Effi_vs_phi_atLeastThreeHits", "Effi;#phi;eff",24,-3.15,3.15);
    eff_R_atLeastOne = new TEfficiency("Effi_vs_R_atLeastOneHit", "Effi;R on layer 1;eff",30,100,700);
    eff_R_atLeastTwo = new TEfficiency("Effi_vs_R_atLeastTwoHits", "Effi;R on layer 1;eff",30,100,700);
    eff_R_atLeastThree = new TEfficiency("Effi_vs_R_atLeastThreHits", "Effi;R on layer 1;eff",30,100,700);

    eff_RPhi_atLeastOne = new TEfficiency("Effi_vs_RPhi_atLeastThreHits", "Effi;#R;#phi;eff",15,100,700,10,-3.15,3.15);


    clusterChiSquare_raw = new TH1D ("Raw_ClusterChi2",";#chi^{2}/n.d.f;a.u.",20,0,5);
    clusterRMS_twoHits = new TH1D ("ClusterTimeRMS_2Hits",";RMS (cl. time) [ns] ;a.u.",60,0,0.3);
    clusterRMS_threeHits = new TH1D ("ClusterTimeRMS_3Hits",";RMS (cl. time) [ns] ;a.u.",60,0,0.3);

    measTimeCluster = new TH1D("ClusterDeltaT",";HGTD Cluster #Delta(T) [ns];a.u.", 50,-1,1);
    measTimeTrackChi2 = new TH1D("TrackTimeChi2Weighted",";Track #Delta(T) [ns];a.u.", 50,-1,1);
    measTimeTrackMean = new TH1D("TrackTimeSimpleAverage",";Track #Delta(T) [ns];a.u.", 50,-1,1);
    measTimeVertexVersusTruth = new TH2D("TrackTimeVertexVersusTruth",";Track #Delta(T) [ns]; True prod time [ns];a.u.", 100,-1,1,100,-1,1);
    measDeltaTimeVertexVersusTruth = new TH1D("TrackTimeVertexMinusTruth",";Track #Delta(T) - True prod time [ns];a.u.", 2000,-1,1);

    ATH_CHECK( m_histSvc->regHist("/HGTDTimeExtension/Raw_NHits",nHits_raw));

    ATH_CHECK( m_histSvc->regGraph("/HGTDTimeExtension/Effi_vs_eta_atLeastOneHitASide",reinterpret_cast<TGraph*>(eff_eta_atLeastOneA)));
    ATH_CHECK( m_histSvc->regGraph("/HGTDTimeExtension/Effi_vs_eta_atLeastTwoHitsASide",reinterpret_cast<TGraph*>(eff_eta_atLeastTwoA)));
    ATH_CHECK( m_histSvc->regGraph("/HGTDTimeExtension/Effi_vs_eta_atLeastThreeHitsASide",reinterpret_cast<TGraph*>(eff_eta_atLeastThreeA)));

    ATH_CHECK( m_histSvc->regGraph("/HGTDTimeExtension/Effi_vs_eta_atLeastOneHitCSide",reinterpret_cast<TGraph*>(eff_eta_atLeastOneC)));
    ATH_CHECK( m_histSvc->regGraph("/HGTDTimeExtension/Effi_vs_eta_atLeastTwoHitsCSide",reinterpret_cast<TGraph*>(eff_eta_atLeastTwoC)));
    ATH_CHECK( m_histSvc->regGraph("/HGTDTimeExtension/Effi_vs_eta_atLeastThreeHitsCSide",reinterpret_cast<TGraph*>(eff_eta_atLeastThreeC)));

    ATH_CHECK( m_histSvc->regGraph("/HGTDTimeExtension/Effi_vs_phi_atLeastOneHit",reinterpret_cast<TGraph*>(eff_phi_atLeastOne)));
    ATH_CHECK( m_histSvc->regGraph("/HGTDTimeExtension/Effi_vs_phi_atLeastTwoHits",reinterpret_cast<TGraph*>(eff_phi_atLeastTwo)));
    ATH_CHECK( m_histSvc->regGraph("/HGTDTimeExtension/Effi_vs_phi_atLeastThreeHits",reinterpret_cast<TGraph*>(eff_phi_atLeastThree)));

    ATH_CHECK( m_histSvc->regGraph("/HGTDTimeExtension/Effi_vs_R_atLeastOneHit",reinterpret_cast<TGraph*>(eff_R_atLeastOne)));
    ATH_CHECK( m_histSvc->regGraph("/HGTDTimeExtension/Effi_vs_R_atLeastTwoHits",reinterpret_cast<TGraph*>(eff_R_atLeastTwo)));
    ATH_CHECK( m_histSvc->regGraph("/HGTDTimeExtension/Effi_vs_R_atLeastThreeHits",reinterpret_cast<TGraph*>(eff_R_atLeastThree)));

    ATH_CHECK( m_histSvc->regGraph("/HGTDTimeExtension/Effi_vs_RPhi_atLeastThreHits",reinterpret_cast<TGraph*>(eff_RPhi_atLeastOne)));
    // ATH_CHECK( m_histSvc->regHist("/HGTDTimeExtension/Effi_vs_eta_atLeastTwoHits",eff_eta_atLeastTwo));
    // ATH_CHECK( m_histSvc->regHist("/HGTDTimeExtension/Effi_vs_eta_atLeastThreHits",eff_eta_atLeastThree));
    ATH_CHECK( m_histSvc->regHist("/HGTDTimeExtension/Raw_ClusterChi2",clusterChiSquare_raw));
    ATH_CHECK( m_histSvc->regHist("/HGTDTimeExtension/ClusterTimeRMS_2Hits",clusterRMS_twoHits));
    ATH_CHECK( m_histSvc->regHist("/HGTDTimeExtension/ClusterTimeRMS_3Hits",clusterRMS_threeHits));

    ATH_CHECK( m_histSvc->regHist("/HGTDTimeExtension/ClusterDeltaT",measTimeCluster));
    ATH_CHECK( m_histSvc->regHist("/HGTDTimeExtension/TrackTimeChi2Weighted",measTimeTrackChi2));
    ATH_CHECK( m_histSvc->regHist("/HGTDTimeExtension/TrackTimeSimpleAverage",measTimeTrackMean));
    
    ATH_CHECK( m_histSvc->regHist("/HGTDTimeExtension/TrackTimeVertexVersusTruth",measTimeVertexVersusTruth));
    ATH_CHECK( m_histSvc->regHist("/HGTDTimeExtension/TrackTimeVertexMinusTruth",measDeltaTimeVertexVersusTruth));
   // hgtd_x  = new TH1D("hgtd_x", "HGTD hits X", 50, -10, 10);
   // ATH_CHECK(m_histSvc->regHist("/HGTDTimeExtension/hgtd_x", hgtd_x));
    return StatusCode::SUCCESS;
}

StatusCode Trk::TrackTimingExtensionPlots::finalize() {
    // booooooring, for now!
    return StatusCode::SUCCESS;
}


StatusCode Trk::TrackTimingExtensionPlots::execute() {
    ATH_MSG_DEBUG("On to a new event! ");
    const xAOD::TrackParticleContainer* tracks = 0;
    ATH_CHECK(evtStore()->retrieve(tracks, m_TrackParticleContainer));

    // now loop over the tracks and run the extension
    for (auto track : *tracks) {
        ATH_CHECK(PlotIt(track));
    }  // end of loop over tracks
    ATH_MSG_DEBUG("Done with this event ");

    return StatusCode::SUCCESS;
}

StatusCode Trk::TrackTimingExtensionPlots::PlotIt (const xAOD::TrackParticle* tp){

    auto eta = tp->eta();
    auto phi = tp->phi();
    auto nLayers = m_dec_nHGTDLayers->operator()(*tp);
    auto expectExtension = m_dec_extensionCandidate->operator()(*tp);
    auto haveExtension = m_dec_hasValidExtension->operator()(*tp);
    auto chi2 = m_dec_perLayer_clusterChi2->operator()(*tp);
    auto hasCl = m_dec_perLayer_hasCluster->operator()(*tp);
    auto clRMS = m_dec_rmsTime->operator()(*tp);
    auto R = m_dec_RonLayer0->operator()(*tp);
    auto measClTime = m_dec_perLayer_clusterDeltaT->operator()(*tp);
    double meanTrackTime = m_dec_meanTime->operator()(*tp);
    double meanTrackTimeC2 = m_dec_meanTimeChi2Weighted->operator()(*tp);
    double trueTrackTime = m_dec_trueProdTime->operator()(*tp);
   // auto particle_link = tp.ParticleLink();
    //cout<<"Particle link = "<<particle_link;
    if (!expectExtension) return StatusCode::SUCCESS;

    if (eta > 0){
        eff_eta_atLeastOneA->Fill((nLayers>0), eta); 
        eff_eta_atLeastTwoA->Fill((nLayers>1), eta); 
        eff_eta_atLeastThreeA->Fill((nLayers>2), eta); 
    }
    else {
        eff_eta_atLeastOneC->Fill((nLayers>0), eta); 
        eff_eta_atLeastTwoC->Fill((nLayers>1), eta); 
        eff_eta_atLeastThreeC->Fill((nLayers>2), eta); 
    }

    eff_phi_atLeastOne->Fill((nLayers>0), phi); 
    eff_phi_atLeastTwo->Fill((nLayers>1), phi); 
    eff_phi_atLeastThree->Fill((nLayers>2), phi); 

    eff_R_atLeastOne->Fill((nLayers>0), R); 
    eff_R_atLeastTwo->Fill((nLayers>1), R); 
    eff_R_atLeastThree->Fill((nLayers>2), R); 

    eff_RPhi_atLeastOne->Fill((nLayers>0), R, phi); 

    if (!haveExtension) return StatusCode::SUCCESS;

    measTimeTrackMean->Fill(meanTrackTime);
    measTimeTrackChi2->Fill(meanTrackTimeC2);
    measTimeVertexVersusTruth->Fill(meanTrackTimeC2, trueTrackTime);
    measDeltaTimeVertexVersusTruth->Fill(meanTrackTimeC2 - trueTrackTime);


    nHits_raw->Fill(nLayers);
    for (size_t cl =0; cl < 4; ++cl){
        if (!hasCl[cl]) continue;
        clusterChiSquare_raw->Fill(chi2[cl]);
        measTimeCluster->Fill(measClTime[cl]);
    }
    if (nLayers > 1) clusterRMS_twoHits->Fill(clRMS); 
    if (nLayers > 2) clusterRMS_threeHits->Fill(clRMS); 

    return StatusCode::SUCCESS;
}
