#include "TrkExTools/TrackTimingExtensionAlg.h"

#include "AtlasDetDescr/AtlasDetectorID.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GeoPrimitives/GeoPrimitives.h"
#include "InDetIdentifier/PixelID.h"
#include "InDetIdentifier/SCT_ID.h"
#include "InDetIdentifier/SiliconID.h"
#include "InDetRIO_OnTrack/PixelClusterOnTrack.h"
#include "InDetReadoutGeometry/SiDetectorElement.h"
#include "TrkDetDescrUtils/BinUtility.h"
#include "TrkDetDescrUtils/BinnedArray1D1D.h"
#include "TrkGeometry/PlaneLayer.h"
#include "TrkSurfaces/RectangleBounds.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTruth/TruthVertex.h"
#include "TLorentzVector.h"
#include "TMath.h"
using namespace std;
namespace Trk {
	typedef std::pair<SharedObject<const Surface>, Amg::Vector3D> SurfaceOrderPosition;
}

Trk::TrackTimingExtensionAlg::TrackTimingExtensionAlg(const std::string& name, ISvcLocator* pSvcLocator) :
	AthAlgorithm(name, pSvcLocator),
	m_extrapolator("Trk::Extrapolator/AtlasExtrapolator"),
	m_beamSpotSvc("BeamCondSvc", name),
	m_updator("Trk::KalmanUpdator/KalmanUpdator"),
	// m_trackSelector(""),
	m_HGTDClusterContainer("HGTD_TimedSingleClusters"),
	m_TrackParticleContainer("InDetTrackParticles"),
	m_deco_name("HGTD_Clusters"),
	m_pixelID(nullptr),
	m_stripsID(nullptr),
	m_pixelManager(nullptr),
	m_stripsManager(nullptr),
	m_clusters(nullptr),
	m_sdoCollection(nullptr),
	m_hardScatterEvent(nullptr),
	m_layerOffset(5),
	m_bins(140),
	m_min(-700.),
	m_max(700.),
	m_dec_trackCategory(nullptr),
	m_dec_extensionCandidate(nullptr),
	m_dec_hasValidExtension(nullptr),
	m_dec_nHGTDLayers(nullptr),
	m_dec_nPossibleHGTDLayers(nullptr),
	m_dec_nMissedHGTDLayers(nullptr),
	m_dec_trueProdTime(nullptr),
	m_dec_meanTime(nullptr),
	m_dec_meanTimeChi2Weighted(nullptr),
	m_dec_rmsTime(nullptr),
	m_dec_RonLayer0(nullptr),
	m_dec_perLayer_hasCluster(nullptr),
	m_dec_perLayer_clusterChi2(nullptr),
	m_dec_perLayer_clusterR(nullptr),
	m_dec_perLayer_clusterRawTime(nullptr),
	m_dec_perLayer_clusterT0(nullptr),
	m_dec_perLayer_clusterDeltaT(nullptr),
	m_dec_perLayer_clusterX(nullptr),
	m_dec_perLayer_clusterY(nullptr),
	m_dec_perLayer_clusterZ(nullptr),
	m_dec_perLayer_clusterTruthClassification(nullptr),
	m_dec_perLayer_clusterIsMerged(nullptr),
	m_dec_perLayer_clusterIsShadowing(nullptr),
	m_dec_perLayer_expectCluster(nullptr),
	m_histSvc("THistSvc", name)
{
	// m_dec_perLayer_ClusterLink(nullptr) {
	declareProperty("Extrapolator", m_extrapolator);
	declareProperty("BeamCondSvc", m_beamSpotSvc);
	declareProperty("Updator", m_updator);
	// declareProperty("TrackSelector", m_trackSelector);
	declareProperty("InputClusterContainer", m_HGTDClusterContainer);
	declareProperty("InputTrackParticleContainer", m_TrackParticleContainer);
	declareProperty("DecorationNamePrefix", m_deco_name);
	// chi2/ndf < 5 by default
	declareProperty("ClusterAssociationChiSquareMax", m_cut_chi2_ext = 5.0);
	// 1 ns default window
	declareProperty("ClusterAssociationTimingWondow", m_dT_clusterSelection = 1.);
	// start by requiring just one cluster...
	declareProperty("MinClustersForExtension", m_nClus_min_ext = 1);

}

Trk::TrackTimingExtensionAlg::~TrackTimingExtensionAlg() {}

StatusCode Trk::TrackTimingExtensionAlg::initialize() {
	// Grab PixelID helper
	if (detStore()->retrieve(m_pixelID, "PixelID").isFailure()) {
		return StatusCode::FAILURE;
	}

	if (detStore()->retrieve(m_pixelManager, "Pixel").isFailure()) {
		return StatusCode::FAILURE;
	}

	if (detStore()->retrieve(m_stripsID, "SCT_ID").isFailure()) {
		return StatusCode::FAILURE;
	}

	if (detStore()->retrieve(m_stripsManager, "SCT").isFailure()) {
		return StatusCode::FAILURE;
	}

	ATH_CHECK(m_extrapolator.retrieve());
	ATH_CHECK(m_beamSpotSvc.retrieve());
	ATH_CHECK(m_updator.retrieve());
	ATH_CHECK(m_histSvc.retrieve());// Shahzad Added
	// ATH_CHECK(m_trackSelector.retrieve());
	ATH_CHECK(populateSurfaceArrays());
	//-------------
	//   int m_Ncluster = 0;// number of cluster per event
	// Histograms Initailization
	hgtd_nclusters = new TH1D("hgtd_nclusters", "Nclusters", 30, 0, 40);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_nclusters", hgtd_nclusters));

	hgtd_npixles = new TH1D("hgtd_npixles", "NPixels", 30, 0, 40);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_npixles", hgtd_npixles));

	hgtd_npixles_prim = new TH1D("hgtd_npixles_prim", "NPixels_prim", 5, 0, 5);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_npixles_prim", hgtd_npixles_prim));

	hgtd_npixles_sec = new TH1D("hgtd_npixles_sec", "NPixels_Sec", 30, 0, 40);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_npixles_sec", hgtd_npixles_sec));

	// HGTD clusters
	hgtd_layer  = new TH1D("hgtd_layer", "HGTD layers", 50, 0, 10);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_layer", hgtd_layer));

	hgtd_x  = new TH1D("hgtd_x", "HGTD hits X", 50, -1000, 1000);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtdx", hgtd_x));

	hgtd_y  = new TH1D("hgtd_y", "HGTD hits y", 50, -1000, 1000);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtdy", hgtd_y));

	hgtd_z  = new TH1D("hgtd_z", "HGTD hits z", 100, 3400, 3500);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtdz", hgtd_z));

	hgtd_r  = new TH1D("hgtd_r", "HGTD hits r", 50, 100, 1010);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtdr", hgtd_r));

	hgtd_xy  = new TH2D("hgtd_xy", "HGTD hits XY", 50, -1000, 1000, 50, -1000, 1000);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtdxy", hgtd_xy));

	hgtd_eta  = new TH1D("hgtd_eta", "HGTD hits Eta", 50, 2, 5);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_eta", hgtd_eta));  

	hgtd_phi  = new TH1D("hgtd_phi", "HGTD hits phi", 50, -4.0, 4.0);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_phi", hgtd_phi));         
 
        hgtd_clus_time = new  TH1D("hgtd_clus_time", "HGTD cluster time", 50, 10.5, 13.0);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_clus_time", hgtd_clus_time));

        hgtd_det_element_time = new TH1D("hgtd_det_element_time", "time of flight to the detector element", 30, 11.4, 11.9);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_det_element_time", hgtd_det_element_time));   	
       
// Histograms for Primary particles
	hgtd_x_prim  = new TH1D("hgtd_x_prim", "HGTD hits X_prim", 50, -1000, 1000);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtdx_prim", hgtd_x_prim));

	hgtd_y_prim  = new TH1D("hgtd_y_prim", "HGTD hits y_prim", 50, -1000, 1000);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtdy_prim", hgtd_y_prim));

	hgtd_z_prim  = new TH1D("hgtd_z_prim", "HGTD hits z_prim", 100, 3400, 3500);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtdz_prim", hgtd_z_prim));

	hgtd_r_prim  = new TH1D("hgtd_r_prim", "HGTD hits r_prim", 50, 100, 1010);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtdr_prim", hgtd_r_prim));

	hgtd_xy_prim  = new TH2D("hgtd_xy_prim", "HGTD hits XY_prim", 50, -1000, 1000, 50, -1000, 1000);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtdxy_prim", hgtd_xy_prim));

	hgtd_eta_prim  = new TH1D("hgtd_eta_prim", "HGTD hits Eta_prim", 50, 2, 5);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_eta_prim", hgtd_eta_prim));

	hgtd_phi_prim  = new TH1D("hgtd_phi_prim", "HGTD hits phi_prim", 50, -4.0, 4.0);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_phi_prim", hgtd_phi_prim));

	hgtd_layer_prim = new TH1D("hgtd_layer_prim", "HGTD layers_prim ", 50, 0, 10);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_layer_prim", hgtd_layer_prim )); 

 	hgtd_time_prim = new TH1D("hgtd_time_prim", "primary PArticles time", 50, 10.5, 13);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_time_prim", hgtd_time_prim));

        hgtd_time_window_prim = new TH1D("hgtd_time_window_prim", "time difference between TOF and deposit time", 50, -1.0, 1.5);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_time_window_prim", hgtd_time_window_prim));
	// Production vertices of particles

	hgtd_vtx_prim  = new TH1D("hgtd_vtx_prim", "Prim particles Production vertex X", 50, -0.1, 0.1);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_vtx_prim", hgtd_vtx_prim));

	hgtd_vty_prim  = new TH1D("hgtd_vty_prim", "Prim particles Production vertex Y", 50, -0.1, 0.1);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_vty_prim", hgtd_vty_prim));

	hgtd_vtz_prim  = new TH1D("hgtd_vtz_prim", "Prim particles Production vertex Z", 50, -200, 200);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_vtz_prim", hgtd_vtz_prim));

	hgtd_vtr_prim  = new TH1D("hgtd_vtr_prim", "Prim particles Production vertex r", 50, 0, 0.06);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_vtr_prim", hgtd_vtr_prim));

	hgtd_vtrz_prim  = new TH2D("hgtd_vtrz_prim", "Prim particles Production vertex rz", 50, -200, 200, 50, 0, 0.06);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_vtrz_prim", hgtd_vtrz_prim));
	// -------------%%%%%%%%%%%%%%%%%%%%%%%%%%%---------------------------------- 
	//-------------Secondary Vertices particle-----------------------------------

	hgtd_x_sec  = new TH1D("hgtd_x_sec", "HGTD Sec hits X", 50, -1000, 1000);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtdx_sec", hgtd_x_sec));

	hgtd_y_sec  = new TH1D("hgtd_y_sec", "HGTD hits y_sec", 50, -1000, 1000);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtdy_sec", hgtd_y_sec));

	hgtd_z_sec  = new TH1D("hgtd_z_sec", "HGTD hits z_sec", 100, 3400, 3500);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtdz_sec", hgtd_z_sec));

	hgtd_r_sec  = new TH1D("hgtd_r_sec", "HGTD hits r_sec", 50, 100, 1010);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtdr_sec", hgtd_r_sec));

	hgtd_xy_sec  = new TH2D("hgtd_xy_sec", "HGTD hits XY_sec", 50, -1000, 1000, 50, -1000, 1000);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtdxy_sec", hgtd_xy_sec));

	hgtd_eta_sec  = new TH1D("hgtd_eta_sec", "HGTD hits Eta_sec", 50, 2, 5);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_eta_sec", hgtd_eta_sec));

	hgtd_phi_sec  = new TH1D("hgtd_phi_sec", "HGTD hits phi_sec", 50, -4.0, 4.0);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_phi_sec", hgtd_phi_sec));

	hgtd_layer_sec = new TH1D("hgtd_layer_sec", "HGTD layers_sec", 50, 0, 10);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_layer_sec", hgtd_layer_sec));

	hgtd_time_sec = new TH1D("hgtd_time_sec", "Secondaries PArticles time", 50, 10.5, 13);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_time_sec", hgtd_time_sec));

        hgtd_time_window_sec = new TH1D("hgtd_time_window_sec", "time difference between TOF and deposit time", 50, -1.0, 1.5);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_time_window_sec", hgtd_time_window_sec));

        hgtd_deltaR_sec  = new TH1D("hgtd_deltaR_sec", "DeltaR between Secondaries hits", 50 , 0, 1000);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_deltaR_sec", hgtd_deltaR_sec));

	hgtd_deltaR_sec_L1  = new TH1D("hgtd_deltaR_sec_L1", "DeltaR between Secondaries hits Layer_1", 50 , 0, 1000);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_deltaR_sec_L1", hgtd_deltaR_sec_L1));

	hgtd_x_avg_sec = new TH1D("hgtd_x_avg_sec", " Average Sec HGTD x Hits", 50, -1000.0, 1000.0);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_x_avg_sec", hgtd_x_avg_sec));

	hgtd_y_avg_sec = new TH1D("hgtd_y_avg_sec", " Average Sec HGTD y Hits", 50, -1000.0, 1000.0);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_y_avg_sec", hgtd_y_avg_sec));

	hgtd_dx_avg_sec = new  TH1D("hgtd_dx_avg_sec", "Sec Delta X between Average hits and hits in a cluster", 50, -1000.0, 1000.0);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_dx_avg_sec", hgtd_dx_avg_sec));

	hgtd_dy_avg_sec = new  TH1D("hgtd_dy_avg_sec", "Sec Delta Y between Average hits and hits in a cluster", 50, -1000.0, 1000.0);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_dy_avg_sec",  hgtd_dy_avg_sec));

	hgtd_dR_avg_sec = new  TH1D("hgtd_dR_avg_sec", "Sec Delta R between Average hits  and hits in a cluster", 50, 0.0, 1000.0);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_dR_avg_sec",  hgtd_dR_avg_sec));
        
	/// Angular study

        hgtd_eta_avg_sec = new  TH1D("hgtd_eta_avg_sec", "Sec Avg Eta ", 50, 2.3, 4.0);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_eta_avg_sec",   hgtd_eta_avg_sec));

	hgtd_phi_avg_sec = new  TH1D("hgtd_phi_avg_sec", "Sec Avg phi ", 50, -3.0, 3.5);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_phi_avg_sec",   hgtd_phi_avg_sec));

        hgtd_deta_avg_sec =  new  TH1D("hgtd_deta_avg_sec", "Sec Avg Delta Eta ", 50, 0, 1);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_deta_avg_sec",   hgtd_deta_avg_sec));

        hgtd_dphi_avg_sec  = new  TH1D("hgtd_dphi_avg_sec", "Sec Avg Delta phi ", 50, 0, 4);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_dphi_avg_sec", hgtd_dphi_avg_sec));
        
        hgtd_delta_R_avg_sec = new  TH1D("hgtd_delta_R_avg_sec", "Sec Avg Delta R ", 50, 0, 5);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_delta_R_avg_sec", hgtd_delta_R_avg_sec));
        /// Layer 1

        hgtd_x_avg_sec_L1 = new TH1D("hgtd_x_avg_sec_L1", " Average Sec HGTD x_L1 Hits", 50, -1000.0, 1000.0);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_x_avg_sec_L1", hgtd_x_avg_sec_L1));

        hgtd_y_avg_sec_L1 = new TH1D("hgtd_y_avg_sec_L1", " Average Sec HGTD y_L1 Hits", 50, -1000.0, 1000.0);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_y_avg_sec_L1", hgtd_y_avg_sec_L1));

        hgtd_dx_avg_sec_L1 = new  TH1D("hgtd_dx_avg_sec_L1", "Sec Delta X_L1 between Average hits and and hits in a cluster", 50, -1000.0, 1000.0);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_dx_avg_sec_L1", hgtd_dx_avg_sec_L1));

        hgtd_dy_avg_sec_L1 = new  TH1D("hgtd_dy_avg_sec_L1", "Sec Delta Y_L1 between Average hits and and hits in a cluster", 50, -1000.0, 1000.0);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_dy_avg_sec_L1",  hgtd_dy_avg_sec_L1));

        hgtd_dR_avg_sec_L1 = new  TH1D("hgtd_dR_avg_sec_L1", "Sec Delta R_L1 between Average hits and and hits in a cluster", 50, 0.0, 1000.0);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_dR_avg_sec_L1",  hgtd_dR_avg_sec_L1));

       	hgtd_eta_avg_sec_L1 = new  TH1D("hgtd_eta_avg_sec_L1", "Sec Avg Eta_L1 ", 50, 2.3, 4.0);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_eta_avg_sec_L1",   hgtd_eta_avg_sec_L1));

        hgtd_phi_avg_sec_L1 = new  TH1D("hgtd_phi_avg_sec_L1", "Sec Avg phi_L1 ", 50, -3.0, 3.5);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_phi_avg_sec_L1",   hgtd_phi_avg_sec_L1));

        hgtd_deta_avg_sec_L1 =  new  TH1D("hgtd_deta_avg_sec_L1", "Sec Avg Delta Eta_L1 ", 50, 0, 1);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_deta_avg_sec_L1",   hgtd_deta_avg_sec_L1));

        hgtd_dphi_avg_sec_L1  = new  TH1D("hgtd_dphi_avg_sec_L1", "Sec Avg Delta phi_L1 ", 50, 0, 4);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_dphi_avg_sec_L1", hgtd_dphi_avg_sec_L1));

        hgtd_delta_R_avg_sec_L1 = new  TH1D("hgtd_delta_R_avg_sec_L1", "Sec Avg Delta R_L1 ", 50, 0, 5);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_delta_R_avg_sec_L1", hgtd_delta_R_avg_sec_L1));

       // Production vertices Histograms for secondary particless
	hgtd_vtx_sec  = new TH1D("hgtd_vtx_sec", "Sec particles Production vertex X", 50, -1000, 1000);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_vtx_sec", hgtd_vtx_sec));

	hgtd_vty_sec  = new TH1D("hgtd_vty_sec", "Sec particles Production vertex Y", 50, -1000, 1000);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_vty_sec", hgtd_vty_sec));

	hgtd_vtz_sec  = new TH1D("hgtd_vtz_sec", "Sec particles Production vertex Z", 50, 0, 4000);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_vtz_sec", hgtd_vtz_sec));

	hgtd_vtr_sec  = new TH1D("hgtd_vtr_sec", "Sec particles Production vertex r", 50, 0, 1000);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_vtr_sec", hgtd_vtr_sec));

	hgtd_vtrz_sec  = new TH2D("hgtd_vtrz_sec", "Sec particles Production vertex rz", 50, 0, 4000, 50, 0, 1000);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_vtrz_sec", hgtd_vtrz_sec));

	// DelTa particles histos
	hgtd_deltaR_deltas  = new TH1D("hgtd_deltaR_deltas", "Distance between deltas hits", 50 , 0, 1000);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_deltaR_deltas", hgtd_deltaR_deltas));

	hgtd_deltaR_deltas_L1  = new TH1D("hgtd_deltaR_deltas_L1", "Distance between deltas hits Layer_1", 50 , 0, 1000);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_deltaR_deltas_L1", hgtd_deltaR_deltas_L1));

	hgtd_x_avg_deltas = new TH1D("hgtd_x_avg_deltas", " Average deltas HGTD x Hits", 50, -1000.0, 1000.0);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_x_avg_deltas", hgtd_x_avg_deltas));

	hgtd_y_avg_deltas = new TH1D("hgtd_y_avg_deltas", " Average deltas HGTD y Hits", 50, -1000.0, 1000.0);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_y_avg_deltas", hgtd_y_avg_deltas));

	hgtd_dx_avg_deltas = new  TH1D("hgtd_dx_avg_deltas", "deltas Delta X between Average hits and and hits in a cluster", 50, -1000.0, 1000.0);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_dx_avg_deltas", hgtd_dx_avg_deltas));

	hgtd_dy_avg_deltas = new  TH1D("hgtd_dy_avg_deltas", "deltas Delta Y between Average hits and and hits in a cluster", 50, -1000.0, 1000.0);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_dy_avg_deltas",  hgtd_dy_avg_deltas));

	hgtd_dR_avg_deltas = new  TH1D("hgtd_dR_avg_deltas", "deltas dR between Average hits and and hits in a cluster", 50, 0.0, 1000.0);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_dR_avg_deltas",  hgtd_dR_avg_deltas));
        
        hgtd_time_deltas = new TH1D("hgtd_time_deltas", "deltas PArticles time", 50, 10.5, 13);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_time_deltas", hgtd_time_deltas));

        hgtd_time_window_deltas = new TH1D("hgtd_time_window_deltas", "deltas time difference between TOF and deposit time", 50, -1.0, 1.5);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_time_window_deltas", hgtd_time_window_deltas));

        // Angular study
         hgtd_eta_avg_deltas = new  TH1D("hgtd_eta_avg_deltas", "deltas Avg Eta ", 50, 2.3, 4.0);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_eta_avg_deltas",   hgtd_eta_avg_deltas));

        hgtd_phi_avg_deltas = new  TH1D("hgtd_phi_avg_deltas", "deltas Avg phi ", 50, -3.0, 3.5);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_phi_avg_deltas",   hgtd_phi_avg_deltas));

        hgtd_deta_avg_deltas =  new  TH1D("hgtd_deta_avg_deltas", "deltas Avg Delta Eta ", 50, 0, 1);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_deta_avg_deltas",   hgtd_deta_avg_deltas));

        hgtd_dphi_avg_deltas  = new  TH1D("hgtd_dphi_avg_deltas", "deltas Avg Delta phi ", 50, 0, 4);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_dphi_avg_deltas", hgtd_dphi_avg_deltas));

        hgtd_delta_R_avg_deltas = new  TH1D("hgtd_delta_R_avg_deltas", "deltas Avg Delta R ", 50, 0, 5);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_delta_R_avg_deltas", hgtd_delta_R_avg_deltas));
        // Layer_1 Deltas
        hgtd_x_avg_deltas_L1 = new TH1D("hgtd_x_avg_deltas_L1", " Average deltas HGTD x_L1 Hits", 50, -1000.0, 1000.0);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_x_avg_deltas_L1", hgtd_x_avg_deltas_L1));

        hgtd_y_avg_deltas_L1 = new TH1D("hgtd_y_avg_deltas_L1", " Average deltas HGTD y_L1 Hits", 50, -1000.0, 1000.0);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_y_avg_deltas_L1", hgtd_y_avg_deltas_L1));

        hgtd_dx_avg_deltas_L1 = new  TH1D("hgtd_dx_avg_deltas_L1", "deltas Delta X_L1 between Average hits and and hits in a cluster", 50, -1000.0, 1000.0);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_dx_avg_deltas_L1", hgtd_dx_avg_deltas_L1));

        hgtd_dy_avg_deltas_L1 = new  TH1D("hgtd_dy_avg_deltas_L1", "deltas Delta Y_L1 between Average hits and and hits in a cluster", 50, -1000.0, 1000.0);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_dy_avg_deltas_L1",  hgtd_dy_avg_deltas_L1));

        hgtd_dR_avg_deltas_L1 = new  TH1D("hgtd_dR_avg_deltas_L1", "deltas dR_L1 between Average hits and and hits in a cluster", 50, 0.0, 1000.0);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_dR_avg_deltas_L1",  hgtd_dR_avg_deltas_L1));
       
        // In terms of eta and phi
        hgtd_eta_avg_deltas_L1 = new  TH1D("hgtd_eta_avg_deltas_L1", "deltas Avg Eta_L1 ", 50, 2.3, 4.0);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_eta_avg_deltas_L1",   hgtd_eta_avg_deltas_L1));

        hgtd_phi_avg_deltas_L1 = new  TH1D("hgtd_phi_avg_deltas_L1", "deltas Avg phi_L1 ", 50, -3.0, 3.5);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_phi_avg_deltas_L1",   hgtd_phi_avg_deltas_L1));

        hgtd_deta_avg_deltas_L1 =  new  TH1D("hgtd_deta_avg_deltas_L1", "deltas Avg Delta Eta_L1 ", 50, 0, 1);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_deta_avg_deltas_L1",   hgtd_deta_avg_deltas_L1));

        hgtd_dphi_avg_deltas_L1  = new  TH1D("hgtd_dphi_avg_deltas_L1", "deltas Avg Delta phi_L1 ", 50, 0, 4);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_dphi_avg_deltas_L1", hgtd_dphi_avg_deltas_L1));

        hgtd_delta_R_avg_deltas_L1 = new  TH1D("hgtd_delta_R_avg_deltas_L1", "deltas Avg Delta R_L1 ", 50, 0, 5);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_delta_R_avg_deltas_L1", hgtd_delta_R_avg_deltas_L1));
    
	//-----------------------Track Histograms-----------------------------------------------

	hgtd_ntrk = new TH1D("hgtd_ntrk", "Number of Tracks Per Event", 50, 0, 6);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_ntrk", hgtd_ntrk));

        hgtd_trk_pt = new TH1D("hgtd_trk_pt", "Tracks Pt ", 50, 0, 10000);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_trk_pt", hgtd_trk_pt));	

	hgtd_trk_eta = new TH1D("hgtd_trk_eta", "Tracks Eta ", 50, 2, 5);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_trk_eta", hgtd_trk_eta));

	hgtd_trk_phi = new TH1D("hgtd_trk_phi", "Tracks phi ", 50, -4.0, 4.0);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_trk_phi", hgtd_trk_phi));

       //------------------------ Production Vertex Info--------------------------------

	prod_vertex_bcod_prim = new TH1D("prod_vertex_bcod_prim", "Barcode for Prim vetices particle", 20, -10 , 10);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/prod_vertex_bcod_prim", prod_vertex_bcod_prim));

	prod_vertex_bcod_sec = new TH1D("prod_vertex_bcod_sec", "Barcode for Sec vetices particle", 100, -20000 , -21000);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/prod_vertex_bcod_sec", prod_vertex_bcod_sec));

	//----------------------------Test-----------------------------------------------------------------------
	h1 = new TH1D("h1", "test histogram for t0", 50, 10.5 , 12.5);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/h1", h1));

        h2 = new TH1D("h2", "test histogram for deltaR ", 40, 0 , 8);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/h2", h2));
        //--------------------------------------**********************-----------
	hgtd_cluster_perEvent = new TH1D("hgtd_cluster_perEvent", "Number of cluster per event", 20, 0 , 20);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_cluster_perEvent", hgtd_cluster_perEvent));

	hgtd_pixel_perEvent = new TH1D("hgtd_pixel_perEvent", "Number of pixels per event", 20, 0 , 20);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_pixel_perEvent",  hgtd_pixel_perEvent));

	hgtd_deposits_perEvent = new TH1D("hgtd_deposits_perEvent", "Number of Deposits per event", 20, 0 , 20);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_deposits_perEvent", hgtd_deposits_perEvent));

	hgtd_deposits_perEvent_prim = new TH1D("hgtd_deposits_perEvent_prim", "Number of Primary Deposits per event", 6, 0 , 6);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_deposits_perEvent_prim", hgtd_deposits_perEvent_prim));

	hgtd_deposits_perEvent_sec = new TH1D("hgtd_deposits_perEvent_sec", "Number of Secondary Deposits per event", 20, 0 , 20);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_deposits_perEvent_sec", hgtd_deposits_perEvent_sec));

	hgtd_deposits_perEvent_deltas = new TH1D("hgtd_deposits_perEvent_Deltas", "Number of Deltas Deposits per event", 30, 0 , 30);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_deposits_perEvent_deltas", hgtd_deposits_perEvent_deltas));

	hgtd_evt_inclusive =  new TH1D("hgtd_evt_inclusive", "inclusiveness in Events", 7, 0 , 7);
	ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_evt_inclusive", hgtd_evt_inclusive));

	//-----------------------Pixel Info---------------------------------------------------------------
	hgtd_loc_x_Pixel = new TH1D("hgtd_loc_x_Pixel", "pixel local x", 50, -1000 , 1000 );
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_loc_x_Pixel", hgtd_loc_x_Pixel));
	
	hgtd_loc_y_Pixel = new TH1D("hgtd_loc_y_Pixel", "pixel local y", 50, -1000 , 1000 );
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_loc_y_Pixel", hgtd_loc_y_Pixel));
	
	hgtd_loc_z_Pixel = new TH1D("hgtd_loc_z_Pixel", "pixel local z", 50, -50 , 50 );
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_loc_z_Pixel", hgtd_loc_z_Pixel));
        
        hgtd_loc_xy_Pixel = new TH2D("hgtd_loc_xy_Pixel", "pixel local xy", 50, -1000 , 1000, 50, -1000, 1000);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_loc_xy_Pixel", hgtd_loc_xy_Pixel));

	
	hgtd_loc_x_Pixel_L1 = new TH1D("hgtd_loc_x_Pixel_L1", "pixel local x_L1", 50, -1000 , 1000 );
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_loc_x_Pixel_L1", hgtd_loc_x_Pixel_L1));

        hgtd_loc_y_Pixel_L1 = new TH1D("hgtd_loc_y_Pixel_L1", "pixel local y_L1", 50, -1000 , 1000 );
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_loc_y_Pixel_L1", hgtd_loc_y_Pixel_L1));

        hgtd_loc_z_Pixel_L1 = new TH1D("hgtd_loc_z_Pixel_L1", "pixel local z_L1", 50, -50 , 50 );
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_loc_z_Pixel_L1", hgtd_loc_z_Pixel_L1));

        hgtd_loc_xy_Pixel_L1 = new TH2D("hgtd_loc_xy_Pixel_L1", "pixel local xy_L1", 50, -1000 , 1000, 50, -1000, 1000);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_loc_xy_Pixel_L1", hgtd_loc_xy_Pixel_L1));

        //*************************----------------- Track and Hits ------------**************************************
 	
	// Primaries for Single Pi+
        /*
	hgtd_hits_eta_prim = new TH1D("hgtd_hits_eta_prim", "Primaries hits eta", 50, 2.3, 5);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_hits_eta_prim",  hgtd_hits_eta_prim));
   
        hgtd_hits_phi_prim = new TH1D("hgtd_hits_phi_prim", "Primaries hits phi", 50, -3.0, 4);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_hits_phi_prim",  hgtd_hits_phi_prim));

 	hgtd_hits_deta_prim = new TH1D("hgtd_hits_deta_prim", "dEta(hits, track)", 50, -1, 1);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_hits_deta_prim",  hgtd_hits_deta_prim ));

        hgtd_hits_dphi_prim = new TH1D("hgtd_hits_dphi_prim", "dPhi(hits, track)", 50, 0, 1);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_hits_dphi_prim",  hgtd_hits_dphi_prim ));

        hgtd_hits_dR_prim = new TH1D("hgtd_hits_dR_prim", "dR (Primary  hits, track)", 50, 0, 0.7);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_hits_dR_prim",  hgtd_hits_dR_prim));*/
	
	// VBF Primaries
	
        hgtd_hits_eta_prim = new TH1D("hgtd_hits_eta_prim", "Primaries hits eta", 50, 2.3, 5);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_hits_eta_prim",  hgtd_hits_eta_prim));

        hgtd_hits_phi_prim = new TH1D("hgtd_hits_phi_prim", "Primaries hits phi", 50, -3.0, 4);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_hits_phi_prim",  hgtd_hits_phi_prim));

        hgtd_hits_deta_prim = new TH1D("hgtd_hits_deta_prim", "dEta(hits, track)", 50, -4, 4);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_hits_deta_prim",  hgtd_hits_deta_prim ));

        hgtd_hits_dphi_prim = new TH1D("hgtd_hits_dphi_prim", "dPhi(hits, track)", 50, 0, 10);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_hits_dphi_prim",  hgtd_hits_dphi_prim ));

        hgtd_hits_dR_prim = new TH1D("hgtd_hits_dR_prim", "dR (Primary  hits, track)", 50, 0, 10);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_hits_dR_prim",  hgtd_hits_dR_prim)); 
	
 	// Secondaries
        hgtd_hits_eta_sec = new TH1D("hgtd_hits_eta_sec", "Secondaries hits eta", 50, 2.3, 5);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_hits_eta_sec",  hgtd_hits_eta_sec));

        hgtd_hits_phi_sec = new TH1D("hgtd_hits_phi_sec", "Secondaries hits phi", 50, -3.0, 4);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_hits_phi_sec",  hgtd_hits_phi_sec));

        hgtd_hits_deta_sec = new TH1D("hgtd_hits_deta_sec", "dEta(Sec_hits, track)", 50, -2, 2);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_hits_deta_sec",  hgtd_hits_deta_sec ));

        hgtd_hits_dphi_sec = new TH1D("hgtd_hits_dphi_sec", "dPhi(sec_hits, track)", 50, 0, 5);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_hits_dphi_sec",  hgtd_hits_dphi_sec ));

        hgtd_hits_dR_sec = new TH1D("hgtd_hits_dR_sec", "dR ( Sec_hits, track)", 50, 0, 2.0);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_hits_dR_sec",  hgtd_hits_dR_sec));
        // Deltas

        hgtd_hits_eta_deltas = new TH1D("hgtd_hits_eta_deltas", "deltas hits eta", 50, 2.3, 5);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_hits_eta_deltas",  hgtd_hits_eta_deltas));

        hgtd_hits_phi_deltas = new TH1D("hgtd_hits_phi_deltas", "deltas hits phi", 50, -3.0, 4);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_hits_phi_deltas",  hgtd_hits_phi_deltas));

        hgtd_hits_deta_deltas = new TH1D("hgtd_hits_deta_deltas", "dEta(deltas_hits, track)", 50, -2, 2);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_hits_deta_deltas",  hgtd_hits_deta_deltas ));

        hgtd_hits_dphi_deltas = new TH1D("hgtd_hits_dphi_deltas", "dPhi(deltas_hits, track)", 50, 0, 5);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_hits_dphi_deltas",  hgtd_hits_dphi_deltas));

        hgtd_hits_dR_deltas = new TH1D("hgtd_hits_dR_deltas", "dR ( deltas_hits, track)", 50, 0, 2.0);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_hits_dR_deltas",  hgtd_hits_dR_deltas));

	// Matched hits with Tracks
	
	hgtd_match_cluster_n = new TH1D("hgtd_match_cluster_n", "number of cluster matched to track", 30, 0, 30);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_match_cluster_n",   hgtd_match_cluster_n));

	hgtd_match_pixels_n = new TH1D("hgtd_match_pixels_n", "number of pixels matched to track", 30, 0, 30);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_match_pixels_n",   hgtd_match_pixels_n));

	hgtd_match_deposits_n = new TH1D("hgtd_match_deposits_n", "number of deposits  matched to track", 30, 0, 30);
        ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtd_match_deposits_n",   hgtd_match_deposits_n));

	

	//-----------------------End of Histograms------------------------------------------------------------
	// set up our decos
	m_dec_trackCategory = std::make_unique<SG::AuxElement::Decorator<int> >(m_deco_name + "_trackSelectionCategory");

	m_dec_extensionCandidate = std::make_unique<SG::AuxElement::Decorator<char> >(m_deco_name + "_candidateForExtension");

	m_dec_hasValidExtension = std::make_unique<SG::AuxElement::Decorator<char> >(m_deco_name + "_hasValidExtension");

	m_dec_nHGTDLayers = std::make_unique<SG::AuxElement::Decorator<int> >(m_deco_name + "_nHGTDLayers");
	m_dec_nPossibleHGTDLayers = std::make_unique<SG::AuxElement::Decorator<int> >(m_deco_name + "_nPossibleHGTDLayers");
	m_dec_nMissedHGTDLayers = std::make_unique<SG::AuxElement::Decorator<int> >(m_deco_name + "_nMissedHGTDLayers");

	m_dec_trueProdTime = std::make_unique<SG::AuxElement::Decorator<float> >(m_deco_name + "_trueProductionTime");

	m_dec_meanTime = std::make_unique<SG::AuxElement::Decorator<float> >(m_deco_name + "_meanTimeArithmeticMean");

	m_dec_meanTimeChi2Weighted = std::make_unique<SG::AuxElement::Decorator<float> >(m_deco_name + "_meanTimeChi2Weighted");

	m_dec_rmsTime = std::make_unique<SG::AuxElement::Decorator<float> >(m_deco_name + "_rmsTime");

	m_dec_RonLayer0 = std::make_unique<SG::AuxElement::Decorator<float> >(m_deco_name + "_RonLayer0");

	m_dec_perLayer_hasCluster = std::make_unique<SG::AuxElement::Decorator<std::vector<bool> > >(m_deco_name + "_perLayer_hasCluster");

	m_dec_perLayer_clusterChi2 = std::make_unique<SG::AuxElement::Decorator<std::vector<float> > >(m_deco_name + "_perLayer_clusterChi2");

	m_dec_perLayer_clusterR = std::make_unique<SG::AuxElement::Decorator<std::vector<float> > >(m_deco_name + "_perLayer_clusterR");

	m_dec_perLayer_clusterRawTime =
		std::make_unique<SG::AuxElement::Decorator<std::vector<float> > >(m_deco_name + "_perLayer_clusterRawTime");
	m_dec_perLayer_clusterT0 = std::make_unique<SG::AuxElement::Decorator<std::vector<float> > >(m_deco_name + "_perLayer_clusterT0");
	m_dec_perLayer_clusterDeltaT =
		std::make_unique<SG::AuxElement::Decorator<std::vector<float> > >(m_deco_name + "_perLayer_clusterDeltaT");

	m_dec_perLayer_clusterX = std::make_unique<SG::AuxElement::Decorator<std::vector<float> > >(m_deco_name + "_perLayer_clusterX");
	m_dec_perLayer_clusterY = std::make_unique<SG::AuxElement::Decorator<std::vector<float> > >(m_deco_name + "_perLayer_clusterY");
	m_dec_perLayer_clusterZ = std::make_unique<SG::AuxElement::Decorator<std::vector<float> > >(m_deco_name + "_perLayer_clusterZ");
	m_dec_perLayer_clusterTruthClassification = std::make_unique<SG::AuxElement::Decorator<std::vector<int> > > (m_deco_name+"_perLayer_clusterTruthClassification"); 
	m_dec_perLayer_clusterIsMerged = std::make_unique<SG::AuxElement::Decorator<std::vector<bool> > > (m_deco_name+"_perLayer_clusterIsMerged"); 
	m_dec_perLayer_clusterIsShadowing = std::make_unique<SG::AuxElement::Decorator<std::vector<bool> > > (m_deco_name+"_perLayer_clusterIsShadowingTrack"); 
	m_dec_perLayer_expectCluster = std::make_unique<SG::AuxElement::Decorator<std::vector<bool> > > (m_deco_name+"_perLayer_ClusterExists"); 
	// m_dec_perLayer_ClusterLink = std::make_unique<SG::AuxElement::Decorator<std::vector< ElementLink<InDet::PixelClusterContainer> >  > > (m_deco_name+"_perLayer_PixelClusterLink"); 


	return StatusCode::SUCCESS;
	// Histograms Initailization
	//   hgtd_x  = new TH1D("hgtd_x", "HGTD hits X", 50, -10, 10);
	//    ATH_CHECK(m_histSvc->regHist("/TrackTimingExtensionAlg/hgtdx", hgtd_x));

}

StatusCode Trk::TrackTimingExtensionAlg::finalize() {
	// booooooring, for now!
	return StatusCode::SUCCESS;
}

StatusCode Trk::TrackTimingExtensionAlg::populateSurfaceArrays() {
	// Andi and Noemi will implement this? Yes :)

	// Build the BinUtilities using the bins and the extensions defined at construction
	Trk::BinUtility* BinUtilityX = new Trk::BinUtility(m_bins, m_min, m_max, Trk::open, Trk::binX);
	Trk::BinUtility* BinUtilityY = new Trk::BinUtility(m_bins, m_min, m_max, Trk::open, Trk::binY);

	// prepare vectors to store BinUtilities vs Y
	std::vector<Trk::BinUtility*>* subBinUtilitiesY = new std::vector<Trk::BinUtility*>;
	for (int bin = 0; bin < m_bins; bin++) {
		subBinUtilitiesY->push_back(BinUtilityY->clone());
	}

	// now get all the surfaces associated to the HGTD modules for both endcaps
	std::vector<std::vector<const Trk::Surface*> > module_surfaces_per_layer_A(4, std::vector<const Trk::Surface*>());  // positive endcap
	std::vector<std::vector<const Trk::Surface*> > module_surfaces_per_layer_C(4, std::vector<const Trk::Surface*>());  // negative endcap

	const InDetDD::SiDetectorElementCollection* elements = (m_pixelManager->getDetectorElementCollection());

	for (unsigned int el = 0; el < m_pixelID->wafer_hash_max(); el++) {
		IdentifierHash hash(el);
		const InDetDD::SiDetectorElement* element = (*elements)[hash];
		if (not element) continue;
		int barrel_ec(m_pixelID->barrel_ec(element->identify()));
		if (barrel_ec == 0) continue;
		int layer(m_pixelID->layer_disk(element->identify()));
		if (layer < m_layerOffset) continue;

		const Trk::Surface* moduleSurface = &(element->surface());

		if (barrel_ec == 2)
			module_surfaces_per_layer_A.at(layer - m_layerOffset).push_back(moduleSurface);
		else
			module_surfaces_per_layer_C.at(layer - m_layerOffset).push_back(moduleSurface);
	}

	// these are the surfaces to be added to build the BinnedArray
	// --> The completion of the grid is implemented here
	std::vector<std::vector<Trk::SurfaceOrderPosition> > surfaces_A;
	std::vector<std::vector<Trk::SurfaceOrderPosition> > surfaces_C;

	// store the z position of the layers
	std::vector<float> z_position_A;
	std::vector<float> z_position_C;

	for (unsigned int pos_layer = 0; pos_layer < module_surfaces_per_layer_A.size(); pos_layer++) {
		surfaces_A.push_back(std::vector<Trk::SurfaceOrderPosition>());
		surfaces_C.push_back(std::vector<Trk::SurfaceOrderPosition>());
		for (int binx = 0; binx < m_bins; binx++) {
			float pos_x = BinUtilityX->binPosition(binx, 0);  // get the center of the bin;
			for (int biny = 0; biny < m_bins; biny++) {
				float pos_y = BinUtilityY->binPosition(biny, 0);  // get the center of the bin;

				// implementing two loops together since the # layers and bins is the same for both endcaps
				// A side
				float dist = 1e7;
				const Trk::Surface* mySurface_A = nullptr;
				for (unsigned int surf = 0; surf < module_surfaces_per_layer_A.at(pos_layer).size(); surf++) {
					if (binx == 0 and biny == 0 and surf == 0)
						z_position_A.push_back(module_surfaces_per_layer_A.at(pos_layer).at(surf)->center().z());
					float my_pos_x = module_surfaces_per_layer_A.at(pos_layer).at(surf)->center().x();
					float my_pos_y = module_surfaces_per_layer_A.at(pos_layer).at(surf)->center().y();
					float mydist = (pos_x - my_pos_x) * (pos_x - my_pos_x) + (pos_y - my_pos_y) * (pos_y - my_pos_y);
					if (mydist < dist) {
						mySurface_A = module_surfaces_per_layer_A.at(pos_layer).at(surf);
						dist = mydist;
					}
				}
				if (mySurface_A) {
					Amg::Vector3D orderPosition(pos_x, pos_y, mySurface_A->center().z());
					Trk::SharedObject<const Trk::Surface> sharedSurface(mySurface_A, true);
					Trk::SurfaceOrderPosition surfaceOrder(sharedSurface, orderPosition);
					surfaces_A.at(pos_layer).push_back(surfaceOrder);
				}

				// C side
				dist = 1e7;  // resetting distance
				const Trk::Surface* mySurface_C = nullptr;
				for (unsigned int surf = 0; surf < module_surfaces_per_layer_C.at(pos_layer).size(); surf++) {
					if (binx == 0 and biny == 0 and surf == 0)
						z_position_C.push_back(module_surfaces_per_layer_C.at(pos_layer).at(surf)->center().z());
					float my_pos_x = module_surfaces_per_layer_C.at(pos_layer).at(surf)->center().x();
					float my_pos_y = module_surfaces_per_layer_C.at(pos_layer).at(surf)->center().y();
					float mydist = (pos_x - my_pos_x) * (pos_x - my_pos_x) + (pos_y - my_pos_y) * (pos_y - my_pos_y);
					if (mydist < dist) {
						mySurface_C = module_surfaces_per_layer_C.at(pos_layer).at(surf);
						dist = mydist;
					}
				}
				if (mySurface_C) {
					Amg::Vector3D orderPosition(pos_x, pos_y, mySurface_C->center().z());
					Trk::SharedObject<const Trk::Surface> sharedSurface(mySurface_C, true);
					Trk::SurfaceOrderPosition surfaceOrder(sharedSurface, orderPosition);
					surfaces_C.at(pos_layer).push_back(surfaceOrder);
				}
			}
		}
	}

	// create 2D-dimensional BinnedArray
	std::vector<Trk::BinnedArray<Trk::Surface>*> binnedArrays_A;
	std::vector<Trk::BinnedArray<Trk::Surface>*> binnedArrays_C;

	for (unsigned int layer = 0; layer < surfaces_A.size(); layer++) {
		binnedArrays_A.push_back(new Trk::BinnedArray1D1D<Trk::Surface>(surfaces_A.at(layer), BinUtilityX, subBinUtilitiesY));
		binnedArrays_C.push_back(new Trk::BinnedArray1D1D<Trk::Surface>(surfaces_C.at(layer), BinUtilityX, subBinUtilitiesY));

		// check the registered surfaces in the binned array
		// A side
		const std::vector<const Trk::Surface*>& arraySurfaces_A = binnedArrays_A.back()->arrayObjects();

		// checking for :
		//   - empty surface bins
		std::vector<const Trk::Surface*>::const_iterator asurfIter = arraySurfaces_A.begin();
		std::vector<const Trk::Surface*>::const_iterator asurfIterEnd = arraySurfaces_A.end();
		for (; asurfIter != asurfIterEnd; ++asurfIter) {
			if ((*asurfIter)) {
				double r = (*asurfIter)->center().perp();
				double x = (*asurfIter)->center().x();
				double y = (*asurfIter)->center().y();
				double z = (*asurfIter)->center().z();
				ATH_MSG_DEBUG("Checking surface at ==> " << x << ", " << y << ", " << z << ", " << r);
			} else {
				ATH_MSG_WARNING("Null surface defined in BinUtility ArraySurfaces vector");
			}
		}

		// C side
		const std::vector<const Trk::Surface*>& arraySurfaces_C = binnedArrays_C.back()->arrayObjects();

		// checking for :
		//   - empty surface bins
		asurfIter = arraySurfaces_C.begin();
		asurfIterEnd = arraySurfaces_C.end();
		for (; asurfIter != asurfIterEnd; ++asurfIter) {
			if ((*asurfIter)) {
				double r = (*asurfIter)->center().perp();
				double x = (*asurfIter)->center().x();
				double y = (*asurfIter)->center().y();
				double z = (*asurfIter)->center().z();
				ATH_MSG_DEBUG("Checking surface at ==> " << x << ", " << y << ", " << z << ", " << r);
			} else {
				ATH_MSG_WARNING("Null surface defined in BinUtility ArraySurfaces vector");
			}
		}
	}

	// Now creating the plane layers with the constructor
	//     Trk::PlaneLayer::PlaneLayer(Amg::Transform3D* transform,
	//                                 Trk::RectangleBounds* rbounds,
	//                                 Trk::SurfaceArray* surfaceArray,
	//                                 double thickness = 0,
	//                                 Trk::OverlapDescriptor* olap = 0,
	//                                 int laytyp = active)

	// A side
	for (unsigned int layer = 0; layer < binnedArrays_A.size(); layer++) {
		// position & bounds of the active Layer
		Amg::Transform3D* layerTransform = new Amg::Transform3D;
		(*layerTransform) = Amg::Translation3D(0., 0., z_position_A.at(layer));
		Trk::RectangleBounds* rbounds = new Trk::RectangleBounds(m_max, m_max);
		m_layers_A.push_back(new const PlaneLayer(layerTransform, rbounds, binnedArrays_A.at(layer)));
	}

	// C side --> this is rotated around y
	for (unsigned int layer = 0; layer < binnedArrays_C.size(); layer++) {
		// position & bounds of the active Layer
		Amg::Transform3D* layerTransform = new Amg::Transform3D;

		(*layerTransform) = Amg::Translation3D(0., 0., z_position_C.at(layer));
		Trk::RectangleBounds* rbounds = new Trk::RectangleBounds(m_max, m_max);
		m_layers_C.push_back(new const PlaneLayer(layerTransform, rbounds, binnedArrays_C.at(layer)));
	}

	return StatusCode::SUCCESS;
}

std::vector<const Trk::Surface*> Trk::TrackTimingExtensionAlg::getCompatibleSurfaces(const Trk::PlaneLayer* layer,
		const TrackParameters* tp, int nSteps) {
	std::vector<const Trk::Surface*> surfaces;

	if (!layer) {
		ATH_MSG_WARNING("nullptr is not a valid PlaneLayer...");
		return surfaces;
	}
	if (!tp) {
		ATH_MSG_WARNING("Invalid track parameters provided!");
		return surfaces;
	}
	const Trk::BinnedArray<Trk::Surface>* binnedArray = layer->surfaceArray();

	const Trk::TrackParameters* onLayer =
		m_extrapolator->extrapolate(*tp, layer->surfaceRepresentation(), Trk::anyDirection, false);
	if (!onLayer){
		ATH_MSG_WARNING("Extrapolation to a layer failed with nsteps to "<<nSteps<<" and layer "<<layer <<" and TP "<<tp<<" with eta "<<tp->momentum().eta()<<" and pt "<<tp->momentum().perp()<<" and origin at "<<tp->position().x()<<", "<<tp->position().y()<<", "<<tp->position().z());
		return surfaces;
	}
	auto locPosition = onLayer->localPosition();
	delete onLayer; 
	// return the surface corresponding to the locPosition + some of the surfaces around
	float step = (m_max - m_min) / float(m_bins);
	for (int binx = -nSteps; binx <= nSteps; binx++) {
		float pos_x = locPosition.x() + binx * step;
		for (int biny = -nSteps; biny <= nSteps; biny++) {
			float pos_y = locPosition.y() + biny * step;
			const Trk::Surface* surf = binnedArray->object(Amg::Vector2D(pos_x, pos_y));
			if (std::find(surfaces.begin(), surfaces.end(), surf) == surfaces.end()) surfaces.push_back(surf);
		}
	}

	return surfaces;
}

// get the starting point for our extrapolation
const Trk::TrackStateOnSurface* Trk::TrackTimingExtensionAlg::getLastHit(const xAOD::TrackParticle* tp) {
	if (!tp) {
		ATH_MSG_ERROR("invalid track");
		return nullptr;
	}
	auto track = tp->track();
	if (!track) {
		ATH_MSG_ERROR("Failed to obtain Trk::Track");
		return nullptr;
	}
	auto vTSOS = track->trackStateOnSurfaces();
	if (!vTSOS) {
		ATH_MSG_ERROR("Failed to retrieve track state on surfaces");
		return nullptr;
	}
	auto rit_TSOS = vTSOS->rbegin();
	auto rend_TSOS = vTSOS->rend();
	for (; rit_TSOS != rend_TSOS; ++rit_TSOS) {
		auto TSOS = *rit_TSOS;
		if (!TSOS) continue;
		if (TSOS->type(Trk::TrackStateOnSurface::Measurement) && TSOS->trackParameters() && TSOS->measurementOnTrack()) {
			return TSOS;
		}
	}
	return nullptr;
}

Trk::TrackTimingExtensionAlg::trackSelectionOutcome Trk::TrackTimingExtensionAlg::decision(const Trk::TrackStateOnSurface* lastTSOS, const xAOD::TrackParticle* tp) {
	if (!tp) {
		ATH_MSG_ERROR("invalid track");
		return trackSelectionOutcome::outOfAcceptance;
	}

	// get measurment from TSOS
	auto meas = lastTSOS->measurementOnTrack();

	// try to get the ROT then
	const Trk::RIO_OnTrack* rot = dynamic_cast<const Trk::RIO_OnTrack*>(meas);
	if (!rot) {
		ATH_MSG_WARNING("ROT not found");
		return trackSelectionOutcome::outOfAcceptance;
	}

	const Identifier& id = rot->identify();

	uint8_t iPixHits(0), iSctHits(0);
	uint8_t iPixHoles(0), iSCTHoles(0);
	tp->summaryValue(iPixHits, xAOD::numberOfPixelHits);
	int pixhits = iPixHits;

	tp->summaryValue(iSctHits, xAOD::numberOfSCTHits);
	int scthits = iSctHits;

	tp->summaryValue(iPixHoles, xAOD::numberOfPixelHoles);
	int pixholes = iPixHoles;

	tp->summaryValue(iSCTHoles, xAOD::numberOfSCTHoles);
	int sctholes = iSCTHoles;

	ATH_MSG_VERBOSE("iSCTHolesTrack has " << pixhits << " PixHits, " << pixholes << " PixHoles, " << scthits << " StripHits, " << sctholes
			<< " StripHoles, ");

	if (fabs(tp->eta()) < 2.4) return trackSelectionOutcome::outOfAcceptance;
	ATH_MSG_DEBUG("Found a track entering the HGTD acceptance! ");

	if ((pixhits + scthits) < 10) return trackSelectionOutcome::failHitCuts;     // decide on a threshold (can be rather high in forward region)
	if ((iPixHoles + iSCTHoles) > 1) return trackSelectionOutcome::failHitCuts;  // decide on a threshold (can also set individual criteria)

	ATH_MSG_DEBUG("Found a track passing extention hit requirements! ");

	/*
	// add here a possible z0 cut if we really want one?
	double z0 = tp->z0();
	double theta = tp->theta();
	const double trkSinTheta = std::sin(theta);
	// I stop here and we can continue if we really want some of these cuts?
	*/

	if (m_pixelID->is_pixel(id)) {
		// skip barrel hit
		if (m_pixelID->barrel_ec(id) == 0) return trackSelectionOutcome::failLastLayers;
		// demand a hit on either of the last 2 rings in pixels
		if (m_pixelManager->numerology().numRingsForDisk(m_pixelID->layer_disk(id)) - (m_pixelID->eta_module(id) + 1) > 1) {
			ATH_MSG_VERBOSE("last hit on disk:"
					<< m_pixelID->layer_disk(id) << " which is "
					<< m_pixelManager->numerology().numRingsForDisk(m_pixelID->layer_disk(id)) - (m_pixelID->eta_module(id) + 1)
					<< " rings away from last ring");
			return trackSelectionOutcome::failLastLayers;
		}
	} else if (m_stripsID->is_sct(id)) {
		// skip barrel hit
		if (m_stripsID->barrel_ec(id) == 0) return trackSelectionOutcome::failLastLayers;
		// demand a hit on the last EC in strips --> maybe should just hardcode this and simplify?
		if (m_stripsManager->numerology().numDisks() - (m_stripsID->layer_disk(id) + 1) != 0) {
			ATH_MSG_VERBOSE("last hit on disk:" << m_stripsID->layer_disk(id) << " which is "
					<< m_stripsManager->numerology().numDisks() - (m_stripsID->layer_disk(id) + 1)
					<< " rings away from last EC");
			return trackSelectionOutcome::failLastLayers;
		}
	} else {
		ATH_MSG_WARNING("Module is neither Pixel or strips");
		return trackSelectionOutcome::failLastLayers;
	}

	return trackSelectionOutcome::passAll;
}

void Trk::TrackTimingExtensionAlg::findCandidateSurfaces(size_t layer, const Trk::TrackStateOnSurface* currentState,
		std::vector<const Trk::TrackParameters*>& candidates) {
	if (layer > 3) {
		ATH_MSG_WARNING("Requested a layer > 3 for the HGTD");
		return;
	}
	if (!currentState) {
		ATH_MSG_ERROR("Invalid TSOS provided to findCandidateSurfaces");
		return;
	}
	// pick the direction we need
	double pz = currentState->trackParameters()->momentum().z();
	auto theLayer = pz > 0 ? m_layers_A[layer] : m_layers_C[layer];

	// extrapolate
	auto surfs = getCompatibleSurfaces(theLayer, currentState->trackParameters());
	const TrackParameters* tp = nullptr; 
	for (auto surf : surfs) {
		tp = m_extrapolator->extrapolate(*currentState->trackParameters(), *surf, Trk::alongMomentum, false);
		if (tp) {
			;
			candidates.push_back(tp);
		} else {
			ATH_MSG_ERROR("Failed to extrapolate a track to one of the surfaces");
		}
	}
}

const Trk::TrackStateOnSurface* Trk::TrackTimingExtensionAlg::bestAssocPixelCluster(const Trk::TrackParameters& trkPar) {
	const Trk::TrackStateOnSurface* best = nullptr;
	double bestChi2 = -1;
	double ReferenceTime = getTimeFromBeamSpot(trkPar);
	for (auto collection : *m_clusters) {
		// I *think* this is how we pick the clusters belonging to the current surface??
		auto ID = collection->identify();
		if (ID != trkPar.associatedSurface().associatedDetectorElementIdentifier()) continue;

		ATH_MSG_DEBUG("For the current surface, found " << collection->size() << " HGTD clusters");
		ATH_MSG_DEBUG("Extrap. Track local position on surface (" << trkPar.localPosition().x() << ", " << trkPar.localPosition().y()
				<< ")");
		ATH_MSG_DEBUG("Extrap. Track global position on surface ("
				<< trkPar.associatedSurface().localToGlobal(trkPar.localPosition())->x() << ", "
				<< trkPar.associatedSurface().localToGlobal(trkPar.localPosition())->y() << ", "
				<< trkPar.associatedSurface().localToGlobal(trkPar.localPosition())->z() << ")");
		for (auto clus : *collection) {
			// only consider clusters within a time window of the expected arrival time (straight line to BS)
			ATH_MSG_DEBUG("Found a cluster with dT = " << getClusterTime(clus) - ReferenceTime << " ns at local ("
					<< clus->localPosition().x() << "," << clus->localPosition().y() << ") / global ("
					<< clus->globalPosition().x() << ", " << clus->globalPosition().y() << ", "
					<< clus->globalPosition().z() << ")");
			if (fabs(getClusterTime(clus) - ReferenceTime) > m_dT_clusterSelection) continue;
			const Trk::TrackStateOnSurface* tsos = updateWithSurface(trkPar, clus);
			if (!tsos) continue;
			if (!tsos->measurementOnTrack() || !tsos->fitQualityOnSurface()) {
				delete tsos;
				continue;
			}
			double redChi2 = tsos->fitQualityOnSurface()->chiSquared() / tsos->fitQualityOnSurface()->doubleNumberDoF();
			ATH_MSG_DEBUG("Chi2 for this cluster is " << redChi2);
			// for the 'good' clusters, we pick the one yielding the best chi2
			if (!best || redChi2 < bestChi2) {
				ATH_MSG_DEBUG("This is the best cluster so far! ");
				best = tsos;
				bestChi2 = redChi2;
			} else {
				delete tsos;
			}
		}
	}
	return best;
}

const Trk::TrackStateOnSurface* Trk::TrackTimingExtensionAlg::updateWithSurface(const Trk::TrackParameters& trkPar,
		const InDet::PixelCluster* cluster) {
	// directly turn the cluster into a clusterOnTrack
	InDet::PixelClusterOnTrack* clusterOnTrack = new InDet::PixelClusterOnTrack(
			cluster, Trk::LocalParameters(cluster->localPosition()), cluster->localCovariance(), cluster->detectorElement()->identifyHash());

	Trk::FitQualityOnSurface* quality = nullptr;

	const Trk::TrackParameters* pars =
		m_updator->addToState(trkPar, clusterOnTrack->localParameters(), clusterOnTrack->localCovariance(), quality);

	return new Trk::TrackStateOnSurface(clusterOnTrack, pars, quality);
}

StatusCode Trk::TrackTimingExtensionAlg::execute() {
	ATH_MSG_DEBUG("On to a new event! ");
	const xAOD::TrackParticleContainer* tracks = 0;
	ATH_CHECK(evtStore()->retrieve(tracks, m_TrackParticleContainer));

	ATH_CHECK(evtStore()->retrieve(m_clusters, m_HGTDClusterContainer));
	ATH_CHECK(getHardScatterEvent());
	ATH_CHECK(evtStore()->retrieve(m_sdoCollection, "HGTD_SDO_Map"));

	std::array<const Trk::TrackStateOnSurface*, 4> aux_extensionOutcome{nullptr,nullptr,nullptr,nullptr};
	std::vector<const Trk::TrackParameters*> aux_extrapSurfaceCandidates;

	//----------------------Shahzad Added these lines to study the HGTD-Clusters---------------
	int n_pixels=0, n_pixels_prim = 0, n_pixels_sec = 0, ntrk = 0;; 
	m_Ncluster = 0;
        // Track with hits

        int Nmatch_clus = 0;
        int Nmatch_pixel = 0;
        int Nmatch_deposit = 0;   
        //
	n_event= n_event+1 ; 

	int n_event_temp = 0;
	int n_event_temp_deltas = 0;

	int n_cluster_perEvent = 0; int n_pixel_perEvent = 0; int n_depoist_perEvent= 0; int n_depoist_perEvent_prim= 0;  int n_depoist_perEvent_sec= 0;
	int n_depoist_perEvent_deltas= 0;    

	float x_avg_sec; float y_avg_sec; float z_avg_sec; float x_avg_sec_tmp; float y_avg_sec_tmp; float z_avg_sec_tmp;
	float x_avg_deltas; float y_avg_deltas; float z_avg_deltas; float x_avg_deltas_tmp; float y_avg_deltas_tmp; float z_avg_deltas_tmp;        
	
        float x_avg_sec_L1; float y_avg_sec_L1; float z_avg_sec_L1; float x_avg_sec_tmp_L1; float y_avg_sec_tmp_L1; float z_avg_sec_tmp_L1;
        float x_avg_deltas_L1; float y_avg_deltas_L1; float z_avg_deltas_L1; float x_avg_deltas_tmp_L1; float y_avg_deltas_tmp_L1;  float z_avg_deltas_tmp_L1;

        // Angular Study

	float eta_avg_sec; float phi_avg_sec; float eta_avg_sec_tmp; float phi_avg_sec_tmp;
        
        float eta_avg_sec_L1; float phi_avg_sec_L1; float eta_avg_sec_tmp_L1; float phi_avg_sec_tmp_L1;
 
	float eta_avg_deltas; float phi_avg_deltas; float eta_avg_deltas_tmp; float phi_avg_deltas_tmp;
	float eta_avg_deltas_L1; float phi_avg_deltas_L1; float eta_avg_deltas_tmp_L1; float phi_avg_deltas_tmp_L1;

//--------------------------------------intializing the vairables to calculate the averages ---------
	
	x_avg_sec_tmp=0;  // Average values of the x position of the sec
	y_avg_sec_tmp=0;  // Average values of the y position of the sec
        z_avg_sec_tmp=0; //

	x_avg_deltas_tmp=0;  // Average values of the x position of the deltas
	y_avg_deltas_tmp=0;  // Average values of the y position of the deltas
        z_avg_deltas_tmp=0; 
   
        int n_depoist_perEvent_sec_L1= 0;
        x_avg_sec_tmp_L1=0;  // Average values of the x position of the sec
        y_avg_sec_tmp_L1=0;  // Average values of the y position of the sec
        z_avg_sec_tmp_L1=0;

        int n_depoist_perEvent_deltas_L1= 0;
        x_avg_deltas_tmp_L1=0;  // Average values of the x position of the deltas
        y_avg_deltas_tmp_L1=0;  // Average values of the y position of the deltas
        z_avg_deltas_tmp_L1=0;        

        eta_avg_sec_tmp = 0;
        phi_avg_sec_tmp = 0;
        eta_avg_sec_tmp_L1 = 0;
        phi_avg_sec_tmp_L1 = 0;

	eta_avg_deltas_tmp = 0;
	phi_avg_deltas_tmp= 0;

        eta_avg_deltas_tmp_L1 = 0;
        phi_avg_deltas_tmp_L1 = 0;

	//loop over Pixel clusters collections
	for (auto collection : *m_clusters)
	{



		// loop over Clusters  
		for (auto cl : *collection)
		{
			m_Ncluster = m_Ncluster+1;
			auto ID = cl->identify();
			int BEC = m_pixelID->barrel_ec(ID);
			int layer = m_pixelID->layer_disk(ID);
			int eta = m_pixelID->eta_module(ID);
			double x = cl->globalPosition().x();
			double y = cl->globalPosition().y();
			double z = cl->globalPosition().z();
			//cout<<"z position of the cluster is = "<<z<<endl;
			TVector3 v1;
			v1.SetXYZ(x, y, z);
			Double_t    m_eta= v1.PseudoRapidity();
			Double_t   m_phi= v1.Phi();
			// cout<<"Layer = "<<layer<<"\t"<<cl->particleLink()<<endl;
			hgtd_layer->Fill(layer);
			hgtd_eta->Fill(m_eta);
			hgtd_phi->Fill(m_phi);
			hgtd_x->Fill(x);
			hgtd_y->Fill(y);
			hgtd_z->Fill(z);
			hgtd_r->Fill(sqrt(x*x + y*y));
			hgtd_xy->Fill(x,y);
                        hgtd_clus_time->Fill(getClusterTime(cl));
			if(layer == 5)
			{
				n_cluster_perEvent =  n_cluster_perEvent +1;
			}  


			//-------------------------
			// RDOs associated with track //
			auto RDOList = cl->rdoList();
			// pixels in cluster
			for (auto rdoID : RDOList){
				// now look for the RDO in the SDO map
				auto detectorElement = m_pixelManager->getDetectorElement(rdoID);

				float dElement_x = detectorElement->center().x();
			 	float dElement_y = detectorElement->center().y();
				float dElement_z = detectorElement->center().z();
				
				TVector3 v2;
				v2.SetXYZ(dElement_x, dElement_y, dElement_z);
				float  dElement_eta = v2.PseudoRapidity(); 
				float dElement_phi = v2.Phi();				

                                double ToF_prod = detectorElement->center().mag() / Gaudi::Units::c_light;
                                hgtd_det_element_time->Fill(ToF_prod);

				auto pos = m_sdoCollection->find(rdoID);
				if(layer == 5){ n_pixel_perEvent =  n_pixel_perEvent +1;}
				// n_pixels = n_pixels+1;
				for(auto deposit : pos->second.getdeposits() ){
					const HepMcParticleLink& particleLink = deposit.first;
					//pdg id: unique integer ID specifying the particle type
					//barcode: An Integer which uniquely identifies the GenParticle within the event. 
					int barcode = particleLink.barcode();

					if(layer == 5){ n_depoist_perEvent = n_depoist_perEvent +1;}                   

					n_pixels = n_pixels+1; 
					// if (fabs(deposit.second - ToF_prod) > 1.0) continue;
					// Primaries particles 
					if (barcode != 0 && barcode < 200000){
						// hgtd_layer_prim->Fill(layer);
						n_pixels_prim = n_pixels_prim+1;
						if(particleLink.isValid()!=0)
						{
							if(layer == 5){n_depoist_perEvent_prim = n_depoist_perEvent_prim + 1;}

                                                        hgtd_time_prim->Fill(deposit.second);
                                               		hgtd_time_window_prim->Fill(deposit.second - ToF_prod);

							hgtd_eta_prim->Fill(m_eta);
							hgtd_phi_prim->Fill(m_phi);
							hgtd_x_prim->Fill(x);
							hgtd_y_prim->Fill(y);
							hgtd_z_prim->Fill(z);
							hgtd_r_prim->Fill(sqrt(x*x + y*y));
							hgtd_xy_prim->Fill(x,y);
							//cout<<"secondary time = "<<deposit.second<<endl;
							//hgtd_time_sec->Fill(deposit.second);
							// production_vertex(): pointer to the vertex where the particle was produced, can only be set by the vertex
							double x_prim = particleLink->production_vertex()->position().x();
							double y_prim = particleLink->production_vertex()->position().y();
							double z_prim = particleLink->production_vertex()->position().z();
							double r_prim = sqrt(x_prim*x_prim + y_prim*y_prim);

							//       cout<<"Primaries Production vertex barcod = "<< particleLink->production_vertex()->barcode()<<endl;
							prod_vertex_bcod_prim->Fill(particleLink->production_vertex()->barcode());
							hgtd_vtx_prim->Fill(x_prim);
							hgtd_vty_prim->Fill(y_prim);
							hgtd_vtz_prim->Fill(z_prim);
							hgtd_vtr_prim->Fill(r_prim);
							hgtd_vtrz_prim->Fill(r_prim ,z_prim);
							// n_pixels_prim = n_pixels_prim+1;
							hgtd_layer_prim->Fill(layer); 
						}
					}
					// secondary Particles
					if ( barcode > 200000)
					{
						//           hgtd_layer_sec->Fill(layer);
						double x_sec, y_sec, z_sec ;
						x_sec = dElement_x; y_sec =dElement_y; z_sec = dElement_z;
						double x_temp, y_temp;
                                            
						//x_avg_sec_tmp =  x_avg_sec_tmp + x_sec;
						//y_avg_sec_tmp =  y_avg_sec_tmp + y_sec;
						//hgtd_x_avg_sec->Fi(x_avg_sec_tmp);
						//hgtd_y_avg_sec->Fi(y_avg_sec_tmp);
						//hgtd_time_window_seccout<<"secondary time = "<<deposit.second<<endl;
                                                hgtd_time_sec->Fill(deposit.second);
  					  	hgtd_time_window_sec->Fill(deposit.second - ToF_prod);
						n_pixels_sec = n_pixels_sec+1;            
						if(particleLink.isValid()!=0){
                                                        
							x_avg_sec_tmp =  x_avg_sec_tmp + x_sec;
                                                        y_avg_sec_tmp =  y_avg_sec_tmp + y_sec;
							z_avg_sec_tmp =  z_avg_sec_tmp + z_sec;
                                                           
							eta_avg_sec_tmp =  eta_avg_sec_tmp + dElement_eta;
                                                        phi_avg_sec_tmp =  phi_avg_sec_tmp + dElement_phi;
							//cout<<"x_avg_sec_tmp = ("<<x_avg_sec_tmp<<") + ("<<x_sec<<")"<<endl;
							double r_sec = sqrt(particleLink->production_vertex()->position().x()*particleLink->production_vertex()->position().x()+ particleLink->production_vertex()->position().y()*particleLink->production_vertex()->position().y()); 
							hgtd_eta_sec->Fill(m_eta);
							hgtd_phi_sec->Fill(m_phi);
							hgtd_x_sec->Fill(x_sec);
							hgtd_y_sec->Fill(y_sec);
							hgtd_z_sec->Fill(z);
							hgtd_r_sec->Fill(sqrt(x_sec*x_sec + y_sec*y_sec));
							hgtd_xy_sec->Fill(x_sec,y_sec);
							hgtd_vtx_sec->Fill(particleLink->production_vertex()->position().x());
							hgtd_vty_sec->Fill(particleLink->production_vertex()->position().y());
							hgtd_vtz_sec->Fill(particleLink->production_vertex()->position().z());
							hgtd_vtr_sec->Fill(r_sec);
							hgtd_vtrz_sec->Fill(particleLink->production_vertex()->position().z(), r_sec);
							//  n_pixels_sec = n_pixels_sec+1;
							hgtd_layer_sec->Fill(layer);

							if(layer == 5)
							 {
								n_depoist_perEvent_sec_L1 = n_depoist_perEvent_sec_L1 + 1;
                                     				x_avg_sec_tmp_L1 =  x_avg_sec_tmp_L1 + x_sec;                                                   
                                                        	y_avg_sec_tmp_L1 =  y_avg_sec_tmp_L1 + y_sec;
              							z_avg_sec_tmp_L1 =  z_avg_sec_tmp_L1 + z_sec;

								eta_avg_sec_tmp_L1 =  eta_avg_sec_tmp_L1 + dElement_eta;
                                                        	phi_avg_sec_tmp_L1 =  phi_avg_sec_tmp_L1 + dElement_phi;
						         }
                                                        n_depoist_perEvent_sec = n_depoist_perEvent_sec + 1;
							// cout<<"secondaries Production vertex barcod = "<< particleLink->production_vertex()->barcode()<<endl;
							prod_vertex_bcod_sec->Fill(particleLink->production_vertex()->barcode());
							// calculat DeltaR for secondaries
							if(n_event > n_event_temp) // Pick the first hit in the event
							{
								x_temp = x_sec; // store the x position of the first hit in an event 
								y_temp = y_sec; // store the y position of the first hit in an event 
							} 

							if(n_event == n_event_temp)// Allow us to subtract the hits positions from the first hit position
							{
								double dR;
								dR = sqrt((x_temp - x_sec)*(x_temp - x_sec) + (y_temp - y_sec)*(y_temp - y_sec));
								// Use this statement to confirm
								//std::cout<<"x_temp = "<<x_temp<<"\t y_temp = "<<y_temp<<"\t x_sec = "<<x_sec<<"\ty_sec = "<<y_sec<<"\t DeltaR = "<<dR<<std::endl;
								if(dR!=0) // Use this check but found no difference while not using ths Statement
								{
									hgtd_deltaR_sec->Fill(dR);
								} 
								//h1->Fill(dR); // A test Histogram                             
							}
							n_event_temp =  n_event;// write this statment here to subtract the hits position from the first hit in an event 

							// Distance between secondaries for a layer say 1
							if(layer == 5)
							{
								if(n_event > n_event_temp) // Pick the first hit in the event
								{
									x_temp = x_sec; // store the x position of the first hit in an event 
									y_temp = y_sec; // store the y position of the first hit in an event 
								}

								if(n_event == n_event_temp)// Allow us to subtract the hits positions from the first hit position
								{
									double dR;
									dR = sqrt((x_temp - x_sec)*(x_temp - x_sec) + (y_temp - y_sec)*(y_temp - y_sec));
									//cout<<"x_temp - x_sec = ("<<x_temp<<")-("<<x_sec<<")\t y_temp - y_sec= ("<<y_temp<<")-("<<y_sec<<")"<<endl;
									//cout<<"DeltaR_sec_layer1 = "<<dR<<endl;
									hgtd_deltaR_sec_L1->Fill(dR);
								}
								n_event_temp =  n_event;
							}

						}// ParticleLin check


					}// Secondary
					//---------------------------------------Deltas----------------
					//
					if ( barcode ==0)
					{
						float x_delta = dElement_x; float  y_delta = dElement_y; float  z_delta = dElement_z;
						float x_temp_delta, y_temp_delta; 

						x_avg_deltas_tmp =  x_avg_deltas_tmp +  x_delta;
						y_avg_deltas_tmp =  y_avg_deltas_tmp +  y_delta;
						z_avg_deltas_tmp =  z_avg_deltas_tmp +  z_delta;
                                                
						eta_avg_deltas_tmp =  eta_avg_deltas_tmp + dElement_eta;
                                                phi_avg_deltas_tmp =  phi_avg_deltas_tmp + dElement_phi;
						// hgtd_x_avg_deltas->Fi(x_avg_deltas_tmp);
						// hgtd_y_avg_deltas->Fi(y_avg_deltas_tmp);

                                                hgtd_time_deltas->Fill(deposit.second);
                                                hgtd_time_window_deltas->Fill(deposit.second - ToF_prod);
					
						if(layer == 5) 
							{
							  n_depoist_perEvent_deltas_L1  = n_depoist_perEvent_deltas_L1 +1;

							  x_avg_deltas_tmp_L1 =  x_avg_deltas_tmp_L1 +  x_delta;
                                                 	  y_avg_deltas_tmp_L1 =  y_avg_deltas_tmp_L1 +  y_delta;
							  z_avg_deltas_tmp_L1 =  z_avg_deltas_tmp_L1 +  z_delta;	 
 
							  eta_avg_deltas_tmp_L1 =  eta_avg_deltas_tmp_L1 + dElement_eta;
                                                	  phi_avg_deltas_tmp_L1 =  phi_avg_deltas_tmp_L1 + dElement_phi;
							  
							}
                                                n_depoist_perEvent_deltas  = n_depoist_perEvent_deltas +1;
						if(n_event >  n_event_temp_deltas)
						{
							x_temp_delta = x_delta;
							y_temp_delta = y_delta;
						}
						if(n_event ==  n_event_temp_deltas)
						{
							float dR_delta;
							dR_delta = sqrt((x_temp_delta - x_delta)*(x_temp_delta - x_delta)+( y_temp_delta - y_delta)*(y_temp_delta - y_delta));
							hgtd_deltaR_deltas->Fill(dR_delta);

						}
						n_event_temp_deltas =  n_event;


						if(layer == 5)
						{
							if(n_event > n_event_temp_deltas)
							{
								x_temp_delta = x_delta;
								y_temp_delta = y_delta;
							}
							if(n_event ==  n_event_temp_deltas)
							{
								float dR_delta;
								dR_delta = sqrt((x_temp_delta - x_delta)*(x_temp_delta - x_delta)+( y_temp_delta - y_delta)*(y_temp_delta - y_delta));
								hgtd_deltaR_deltas_L1->Fill(dR_delta);
								//cout<<"x_temp_delta - x_delta = ("<<x_temp_delta<<")-("<<x_delta<<")\t y_temp_delta - y_delta = ("<<y_temp_delta<<")-("<<y_delta<<")"<<endl;
								//cout<<"DeltaR_delta_layer1 = "<<dR_delta<<endl;

							}
							n_event_temp_deltas =  n_event;                                       
						}
					}
					//
				}// RDO Loop mean deposit loop
				//n_event_temp =  n_event;        
			}// pixels in cluster loop

			//---------------------------------------- Calculating DeltaR w.r.t average for each variable----------


			//------------------------
			// cout<<"number of cluster in the current event = "<<j<<endl;  

		}
		//      cout<<"number of cluster  = "<<m_Ncluster<<endl; 
		// hgtd_nclusters->Fill(m_Ncluster); 
	}
	//cout<<"number of clusters per Event  = "<<m_Ncluster<<endl; 
	if (m_Ncluster!=0){ hgtd_nclusters->Fill(m_Ncluster);}
	if(n_pixels!=0){
		// cout<<"Number of hist per Event = "<< n_pixels<<endl;
		hgtd_npixles->Fill(n_pixels);
	}
	if(n_pixels_prim !=0)
	{
		//    cout<<"Number of hist per Event = "<< n_pixels_prim<<endl;   
		hgtd_npixles_prim->Fill(n_pixels_prim);
	}
	if(n_pixels_sec !=0)
	{
		hgtd_npixles_sec->Fill(n_pixels_sec);
	}


	//---------------Filling Histos------------------
	//hgtd_cluster_perEvent->Fill(n_cluster_perEvent);
	//hgtd_pixel_perEvent->Fill(n_pixel_perEvent);
	//hgtd_deposits_perEvent->Fill(n_depoist_perEvent);
	if (n_depoist_perEvent_prim!=0)
	{
		hgtd_deposits_perEvent_prim->Fill(n_depoist_perEvent_prim);
	}

	if (n_depoist_perEvent_sec!=0)
	{
		//hgtd_deposits_perEvent_sec->Fill(n_depoist_perEvent_sec);
		x_avg_sec = x_avg_sec_tmp/n_depoist_perEvent_sec;
                hgtd_x_avg_sec->Fill(x_avg_sec);
         
	        y_avg_sec = y_avg_sec_tmp/n_depoist_perEvent_sec;
                hgtd_y_avg_sec->Fill(y_avg_sec); 
		//cout<<"Average x_sec => "<<x_avg_sec<<" = "<<x_avg_sec_tmp<<"/"<<n_depoist_perEvent_sec<<endl;

 		z_avg_sec = z_avg_sec_tmp/n_depoist_perEvent_sec;
		
		eta_avg_sec = eta_avg_sec_tmp/n_depoist_perEvent_sec;
                hgtd_eta_avg_sec->Fill(eta_avg_sec);

                phi_avg_sec = phi_avg_sec_tmp/n_depoist_perEvent_sec;
                hgtd_phi_avg_sec->Fill(phi_avg_sec);

	}
        // secondary in  Layer 1
        if (n_depoist_perEvent_sec_L1!=0)
         {
                hgtd_deposits_perEvent_sec->Fill(n_depoist_perEvent_sec_L1);
                x_avg_sec_L1 = x_avg_sec_tmp_L1/n_depoist_perEvent_sec_L1;
                hgtd_x_avg_sec_L1->Fill(x_avg_sec_L1);

                y_avg_sec_L1 = y_avg_sec_tmp_L1/n_depoist_perEvent_sec_L1;
                hgtd_y_avg_sec_L1->Fill(y_avg_sec_L1);
                
		z_avg_sec_L1 = z_avg_sec_tmp_L1/n_depoist_perEvent_sec_L1; 
	       //cout<<"Average x_sec => "<<x_avg_sec<<" = "<<x_avg_sec_tmp<<"/"<<n_depoist_perEvent_sec<<endl;

		eta_avg_sec_L1 = eta_avg_sec_tmp_L1/n_depoist_perEvent_sec_L1;
                hgtd_eta_avg_sec_L1->Fill(eta_avg_sec_L1);

                phi_avg_sec_L1 = phi_avg_sec_tmp_L1/n_depoist_perEvent_sec_L1;
                hgtd_phi_avg_sec_L1->Fill(phi_avg_sec_L1);
        }
       // Deltas
	if (n_depoist_perEvent_deltas!=0)
	  {
		//hgtd_deposits_perEvent_deltas->Fill(n_depoist_perEvent_deltas);
        
	        x_avg_deltas = x_avg_deltas_tmp/n_depoist_perEvent_deltas;
		hgtd_x_avg_deltas->Fill(x_avg_deltas);
        
		y_avg_deltas = y_avg_deltas_tmp/n_depoist_perEvent_deltas;
		hgtd_y_avg_deltas->Fill(y_avg_deltas);
		
		z_avg_deltas = z_avg_deltas_tmp/n_depoist_perEvent_deltas;
                // cout<<"Average x_deltas = "<<x_avg_deltas<<"\tAverage y_deltas = "<< y_avg_deltas<<endl;
		
		eta_avg_deltas = eta_avg_deltas_tmp/n_depoist_perEvent_deltas;
                hgtd_eta_avg_deltas->Fill(eta_avg_deltas);

                phi_avg_deltas = phi_avg_deltas_tmp/n_depoist_perEvent_deltas;
                hgtd_phi_avg_deltas->Fill(phi_avg_deltas);
         }
        // deltas in Layer 1
         if (n_depoist_perEvent_deltas_L1!=0)
          {
                hgtd_deposits_perEvent_deltas->Fill(n_depoist_perEvent_deltas_L1);

                x_avg_deltas_L1 = x_avg_deltas_tmp_L1/n_depoist_perEvent_deltas_L1;
                hgtd_x_avg_deltas_L1->Fill(x_avg_deltas_L1);

                y_avg_deltas_L1 = y_avg_deltas_tmp_L1/n_depoist_perEvent_deltas_L1;
                hgtd_y_avg_deltas_L1->Fill(y_avg_deltas_L1);
         
	        z_avg_deltas_L1 = z_avg_deltas_tmp_L1/n_depoist_perEvent_deltas_L1;
		
		eta_avg_deltas_L1 = eta_avg_deltas_tmp_L1/n_depoist_perEvent_deltas_L1;
                hgtd_eta_avg_deltas_L1->Fill(eta_avg_deltas_L1);

                phi_avg_deltas_L1 = phi_avg_deltas_tmp_L1/n_depoist_perEvent_deltas_L1;
                hgtd_phi_avg_deltas_L1->Fill(phi_avg_deltas_L1);
                //cout<<"Average x_deltas = "<<x_avg_deltas<<"\tAverage y_deltas = "<< y_avg_deltas<<endl;
          }
        //

	if(n_depoist_perEvent_prim >0 && n_depoist_perEvent_sec==0 && n_depoist_perEvent_deltas==0)

	{
		hgtd_evt_inclusive->Fill(0.5);
	}
	if(n_depoist_perEvent_prim == 0 && n_depoist_perEvent_sec>0 && n_depoist_perEvent_deltas==0)

	{
		hgtd_evt_inclusive->Fill(1.5);
	}
	if(n_depoist_perEvent_prim ==0 && n_depoist_perEvent_sec==0 && n_depoist_perEvent_deltas>0)

	{
		hgtd_evt_inclusive->Fill(2.5);
	}
	if(n_depoist_perEvent_prim >0 && n_depoist_perEvent_sec>0 && n_depoist_perEvent_deltas==0)

	{
		hgtd_evt_inclusive->Fill(3.5);
	}
	if(n_depoist_perEvent_prim ==0 && n_depoist_perEvent_sec>0 && n_depoist_perEvent_deltas>0)

	{
		hgtd_evt_inclusive->Fill(4.5);
	}
	if(n_depoist_perEvent_prim >0 && n_depoist_perEvent_sec==0 && n_depoist_perEvent_deltas>0)

	{
		hgtd_evt_inclusive->Fill(5.5);
	}
	if(n_depoist_perEvent_prim >0 && n_depoist_perEvent_sec>0 && n_depoist_perEvent_deltas>0)

	{
		hgtd_evt_inclusive->Fill(6.5);
	}
        //-------------------------Calulating Distance between secondaries w.r.t there average position in an event
        TVector3 v_sec;
        v_sec.SetXYZ(x_avg_sec, y_avg_sec, z_avg_sec);
        float  dElement_eta_avg_sec = v_sec.PseudoRapidity();
        float dElement_phi_avg_sec = v_sec.Phi(); 

        TVector3 v_deltas;
        v_deltas.SetXYZ(x_avg_deltas, y_avg_deltas, z_avg_deltas);
        float  dElement_eta_avg_deltas = v_deltas.PseudoRapidity();
        float dElement_phi_avg_deltas = v_deltas.Phi(); 

	for (auto collection : *m_clusters)
	      {
                for (auto cl : *collection)
                 {
                    auto RDOList = cl->rdoList();
                    for (auto rdoID : RDOList)
		     {
			auto pos = m_sdoCollection->find(rdoID);
			auto detectorElement = m_pixelManager->getDetectorElement(rdoID);
			float dElement_x = detectorElement->center().x();
                        float dElement_y = detectorElement->center().y();
		        float dElement_z = detectorElement->center().z();	
			// get the time of flight to the detector element 
			
			//double ToF_prod = detectorElement->center().mag() / Gaudi::Units::c_light;
			//hgtd_det_element_time->Fill(ToF_prod);

                        //-------------**********-------------------------
			auto ID = cl->identify();
			int layer = m_pixelID->layer_disk(ID);
			for(auto deposit : pos->second.getdeposits() )
			 {
			   const HepMcParticleLink& particleLink = deposit.first;
			   int barcode = particleLink.barcode();
			   // Secondaries
			   if(barcode > 200000)
                             {
				if(particleLink.isValid()!=0)
                                  {
					float x_sec =  dElement_x;  float y_sec =  dElement_y; 
					float dx_sec = x_sec - x_avg_sec;
					float dy_sec = y_sec - y_avg_sec;
					float dR_sec = sqrt(dx_sec*dx_sec + dy_sec*dy_sec);
  					hgtd_dx_avg_sec->Fill(dx_sec);
					hgtd_dy_avg_sec->Fill(dy_sec);
					hgtd_dR_avg_sec->Fill(dR_sec); 

                                        TVector3 v2;
                                        v2.SetXYZ(dElement_x, dElement_y, dElement_z);
                                        float  dElement_eta = v2.PseudoRapidity();
                                        float dElement_phi = v2.Phi();
					
					//TVector3 v3;
                                       // v3.SetXYZ(x_avg_sec, y_avg_sec, z_avg_sec);
                                       // float  dElement_eta_avg = v3.PseudoRapidity();
                                        //float dElement_phi_avg = v3.Phi();
					
                                        float deta_sec = dElement_eta - dElement_eta_avg_sec;

                                        //cout<<"deta_sec = "<<deta_sec<<"\t and eta_avg_sec= "<<eta_avg_sec<<endl;
 
					//cout<<"dElement_eta_sec = "<<dElement_eta<<"\t dElement_eta_avg_sec = "<<dElement_eta_avg_sec<<endl;
					 
                                        float dphi_sec_tmp = TMath::Abs(dElement_phi - dElement_phi_avg_sec);
                                        if(dphi_sec_tmp>TMath::Pi())
						{

							dphi_sec_tmp = TMath::TwoPi() - dphi_sec_tmp;
				
						}

                                        float dphi_sec =  dphi_sec_tmp;
                                         
                                        float dR_eta_phi = sqrt(deta_sec*deta_sec + dphi_sec*dphi_sec);
                                        hgtd_hits_eta_prim->Fill(dElement_eta);
					hgtd_hits_phi_prim->Fill(dElement_phi);
			                hgtd_deta_avg_sec->Fill(deta_sec);
                                        hgtd_dphi_avg_sec->Fill(dphi_sec);
                                        hgtd_delta_R_avg_sec->Fill(dR_eta_phi);
					

					if(layer==5)
                                          {
					   float x_sec_L1 =  dElement_x;  float y_sec_L1 =  dElement_y;
                                       	   float dx_sec_L1 = x_sec_L1 - x_avg_sec_L1;
                                           float dy_sec_L1 = y_sec_L1 - y_avg_sec_L1;
                                           float dR_sec_L1 = sqrt(dx_sec_L1*dx_sec_L1 + dy_sec_L1*dy_sec_L1);
                                           hgtd_dx_avg_sec_L1->Fill(dx_sec_L1);
                                           hgtd_dy_avg_sec_L1->Fill(dy_sec_L1);
                                           hgtd_dR_avg_sec_L1->Fill(dR_sec_L1);

					   hgtd_deta_avg_sec_L1->Fill(deta_sec);
                                           hgtd_dphi_avg_sec_L1->Fill(dphi_sec);
                                           hgtd_delta_R_avg_sec_L1->Fill(dR_eta_phi);
					  }
				  }
			     }// Secondaries Check 
                           // Delta Particles
                           if(barcode ==0)
                             {
                               // if(particleLink.isValid()!=0)
                                 // {
                                        float x_deltas =  dElement_x;  float y_deltas =  dElement_y;
                                        float dx_deltas = x_deltas - x_avg_deltas;
                                        float dy_deltas = y_deltas - y_avg_deltas;
                                        float dR_deltas = sqrt(dx_deltas*dx_deltas + dy_deltas*dy_deltas);
                                        hgtd_dx_avg_deltas->Fill(dx_deltas);
                                        hgtd_dy_avg_deltas->Fill(dy_deltas);
                                        hgtd_dR_avg_deltas->Fill(dR_deltas);

					TVector3 v2;
                                        v2.SetXYZ(dElement_x, dElement_y, dElement_z);
                                        float  dElement_eta = v2.PseudoRapidity();
                                        float dElement_phi = v2.Phi();

        				//TVector3 v3;
                                        //v3.SetXYZ(x_avg_deltas, y_avg_deltas, z_avg_deltas);
                                       // float  dElement_eta_avg = v3.PseudoRapidity();
                                        //float dElement_phi_avg = v3.Phi();

                                        float deta_deltas = dElement_eta - dElement_eta_avg_deltas;

                                        float dphi_deltas_tmp = TMath::Abs(dElement_phi - dElement_phi_avg_deltas);
                                        if(dphi_deltas_tmp>TMath::Pi())
                                                {

                                                        dphi_deltas_tmp = TMath::TwoPi() - dphi_deltas_tmp;

                                                }

                                        float dphi_deltas =  dphi_deltas_tmp;

                                        float dR_eta_phi = sqrt(deta_deltas*deta_deltas + dphi_deltas*dphi_deltas);
                                        hgtd_deta_avg_deltas->Fill(deta_deltas);
                                        hgtd_dphi_avg_deltas->Fill(dphi_deltas);
                                        hgtd_delta_R_avg_deltas->Fill(dR_eta_phi);

					if(layer==5)
					 {
 						float x_deltas_L1 =  dElement_x;  float y_deltas_L1 =  dElement_y;
                                        	float dx_deltas_L1 = x_deltas_L1 - x_avg_deltas_L1;
                                        	float dy_deltas_L1 = y_deltas_L1 - y_avg_deltas_L1;
                                        	float dR_deltas_L1 = sqrt(dx_deltas_L1*dx_deltas_L1 + dy_deltas_L1*dy_deltas_L1);
                                        	hgtd_dx_avg_deltas_L1->Fill(dx_deltas_L1);
                                        	hgtd_dy_avg_deltas_L1->Fill(dy_deltas_L1);
                                        	hgtd_dR_avg_deltas_L1->Fill(dR_deltas_L1);
				
						hgtd_deta_avg_deltas_L1->Fill(deta_deltas);
                                        	hgtd_dphi_avg_deltas_L1->Fill(dphi_deltas);
                                        	hgtd_delta_R_avg_deltas_L1->Fill(dR_eta_phi);
				      	 }
                                 // }
                             }// Secondaries Check 


		       	 }
		     } 
		 } 
              }    

	//----%%%%%%%%%%%%%%%%%%%%%---------End---------------%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
	//int Nmatch_clus = 0;
	//int Nmatch_pixel = 0; 
	//int Nmatch_deposit = 0;


	// now loop over the tracks and run the extension
	for (auto track : *tracks) {
		double R_extrap = -1;
		ntrk =  ntrk+1;
                
                int Nmatch_clus = 0;
		int Nmatch_pixel = 0; 
		int Nmatch_deposit = 0;

 		double trk_pt = track->pt();
		double trk_eta = track->eta();
		double trk_phi = track->phi();

                cout<<"Track Pt = "<<track->pt()<<endl;

		hgtd_trk_pt->Fill(track->pt());
		hgtd_trk_eta->Fill(trk_eta);
		hgtd_trk_phi->Fill(trk_phi);
                
		// std::cout<<"Track Eta = "<< track->eta()<<std::endl;
		// first, obtain a starting point - the last hit on the existing track
		const Trk::TrackStateOnSurface* theState = getLastHit(track);
		trackSelectionOutcome thedecision = decision(theState, track);
		if (thedecision  == trackSelectionOutcome::outOfAcceptance) {
			ATH_MSG_DEBUG("Decorating the outcome to the track that failed the selection -- NULL decoration");
		}
		else {

			ATH_MSG_DEBUG("This track passed the selection and will be processed for a HGTD extension");
			ATH_MSG_DEBUG("Attempting the extension");
			// now, we perform (up to) 4 update steps
			for (size_t step = 0; step < 4; ++step) {
				ATH_MSG_DEBUG("Now in layer " << step);
				aux_extrapSurfaceCandidates.clear();
				double bestChiSquare = -1;
				const Trk::TrackStateOnSurface* bestState = nullptr;
				// find the HGTD surfaces to consider
				findCandidateSurfaces(step, theState, aux_extrapSurfaceCandidates);
				ATH_MSG_DEBUG("Found " << aux_extrapSurfaceCandidates.size() << " surfaces");

				if (step == 0) {
					// remember the radius at which we enter the HGTD
					if (aux_extrapSurfaceCandidates.size() > 0) {
						R_extrap = aux_extrapSurfaceCandidates.front()
							->associatedSurface()
							.localToGlobal(aux_extrapSurfaceCandidates.front()->localPosition())
							->perp();
					}
				}

				// for each of the potential surfaces, look for associated HGTD clusters
				for (auto parameters : aux_extrapSurfaceCandidates) {
					// on each surface, find the one cluster best matching our track and remember
					// the progressive filter result for this one
					const Trk::TrackStateOnSurface* candidate = bestAssocPixelCluster(*parameters);

					// check if there is a candidate
					if (!candidate) continue;

					// for now, keep the candidate with the best chi2, assuming
					// that any other selection is happening within bestAssocPixelCluster
					double redchi2 = candidate->fitQualityOnSurface()->chiSquared() / candidate->fitQualityOnSurface()->doubleNumberDoF();
					ATH_MSG_DEBUG("Found a candidate for this surface, chi2 is " << redchi2);
					if (!bestState || redchi2 < bestChiSquare) {
						bestChiSquare = redchi2;
						bestState = candidate;
					} else {
						delete candidate;
					}
				}
				/* make this do something reasonable*/
				// for now, we apply a chi2 cut (defaulting at 5).
				// Anything else?
				ATH_MSG_DEBUG("Best candidate for this layer has a chi2 of " << bestChiSquare);
				if (bestChiSquare > m_cut_chi2_ext) {
					ATH_MSG_DEBUG("Extension fails quality cuts");
					delete bestState;
					bestState = nullptr;
				}

				// clean up
				auto theSurf = aux_extrapSurfaceCandidates.begin();
				auto lastSurf = aux_extrapSurfaceCandidates.end();
				for (; theSurf != lastSurf; ++theSurf) {
					delete *theSurf;
				}
				aux_extrapSurfaceCandidates.clear();

				// also write a nullptr here if failed - this gives us the pattern of associations per layer for later study
				aux_extensionOutcome.at(step) = bestState;

				if (!bestState) {
					ATH_MSG_DEBUG("Extension fails in layer " << step);
					// do not 'break' here - there might valid hits later on?
					continue;
				}
				ATH_MSG_DEBUG("Succesful extension to layer " << step);
				// when adding further hits, start from the current state
				theState = bestState;

			}  // end of loop over HGTD layers
		}// end of else statment

		ATH_MSG_DEBUG("Decorating the outcome to the track");
		ATH_CHECK(decorateExtensionOutcome(track, aux_extensionOutcome, R_extrap, thedecision));

		if (thedecision > trackSelectionOutcome::outOfAcceptance && !m_dec_hasValidExtension->operator()(*track)){
			ATH_MSG_DEBUG("====> Zero HGTD Hits! ");
			findCandidateSurfaces(0, theState, aux_extrapSurfaceCandidates);
			findCandidateSurfaces(1, theState, aux_extrapSurfaceCandidates);
			findCandidateSurfaces(2, theState, aux_extrapSurfaceCandidates);
			findCandidateSurfaces(3, theState, aux_extrapSurfaceCandidates);

			auto theSurf = aux_extrapSurfaceCandidates.begin();
			auto lastSurf = aux_extrapSurfaceCandidates.end();
			for (; theSurf != lastSurf; ++theSurf) {
				auto ID = (*theSurf)->associatedSurface().associatedDetectorElementIdentifier();
				int BEC = m_pixelID->barrel_ec(ID);
				int layer = m_pixelID->layer_disk(ID);
				int eta = m_pixelID->eta_module(ID);
				auto loc = (*theSurf)->associatedSurface().globalReferencePoint();
				double x = loc.x();
				double y = loc.y();
				double z = loc.z();
                                
				TVector3 v1;
                                v1.SetXYZ(x, y, z);
                                double  eta_trk_el = v1.PseudoRapidity();
                                double  phi_trk_el = v1.Phi();

				hgtd_loc_x_Pixel->Fill(x);
				hgtd_loc_y_Pixel->Fill(y);
				hgtd_loc_z_Pixel->Fill(z);
				hgtd_loc_xy_Pixel->Fill(x, y);
                             
                                  
				if(layer == 5)
				 {
				     hgtd_loc_x_Pixel_L1->Fill(x);
                                     hgtd_loc_y_Pixel_L1->Fill(y);
                                     hgtd_loc_z_Pixel_L1->Fill(z);
                                     hgtd_loc_xy_Pixel_L1->Fill(x, y);	
			 	 }
				for (auto coll : *m_clusters){
                                for (auto cl: *coll){

					Nmatch_clus = Nmatch_clus + 1;
                                         
					auto ID_cl = cl->identify();
                                        int BEC_cl = m_pixelID->barrel_ec(ID_cl);
                                        int layer_cl = m_pixelID->layer_disk(ID_cl);
                                        int eta_cl = m_pixelID->eta_module(ID_cl);
                                        double x_cl = cl->globalPosition().x();
                                        double y_cl = cl->globalPosition().y();
                                        double z_cl = cl->globalPosition().z();
					// RDOs associated with track //
					auto RDOList = cl->rdoList();
					for (auto rdoID : RDOList)
					   {
					     auto detectorElement = m_pixelManager->getDetectorElement(rdoID);
                                	     float dElement_x = detectorElement->center().x();
                                	     float dElement_y = detectorElement->center().y();
                                	     float dElement_z = detectorElement->center().z(); 
					      
					 
					     Nmatch_pixel  = Nmatch_pixel + 1;			
					     //cout<<"pixel in an event = "<<Nmatch_pixel<<endl;
					     TVector3 v2;
                                	     v2.SetXYZ(dElement_x, dElement_y, dElement_z);
                                	     double  dElement_eta = v2.PseudoRapidity();
                                             double  dElement_phi = v2.Phi();
                                            // cout<<"dElement_eta in track = "<<dElement_eta<<endl;
					     auto pos = m_sdoCollection->find(rdoID);
					     for(auto deposit : pos->second.getdeposits() )
					        {
           					 
        					    
  							Nmatch_deposit = Nmatch_deposit + 1;
					            
					  	 const HepMcParticleLink& particleLink = deposit.first;
					 	 int barcode = particleLink.barcode();
						 // Primaries Particles
						 if (barcode > 0 && barcode < 200000)
	 					  {
	 					   if(particleLink.isValid()!=0)
	  					    {
	                                              float dEta = eta_trk_el - dElement_eta;
				 		      //float dPhi = phi_trk_el - dElement_phi;
						    
						      float dphi_prim_tmp = TMath::Abs(phi_trk_el - dElement_phi);
                                        	      if(dphi_prim_tmp>TMath::Pi())
                                                	{

                                                        	dphi_prim_tmp = TMath::TwoPi() - dphi_prim_tmp;

                                                	}

                                                     float dphi =  dphi_prim_tmp;
                                                      hgtd_hits_deta_prim->Fill(dEta);
						      hgtd_hits_dphi_prim->Fill(dphi);
						    
						      float dr = sqrt(dEta*dEta + dphi*dphi);
						      hgtd_hits_dR_prim->Fill(dr);        	      				      
					       	    }// Particle Link check   
						   }// Primaries Particles

						 // Secondaries Particles
						 if (barcode > 200000)
                                                  {
                                                   if(particleLink.isValid()!=0)
                                                    {
       							 float dEta = eta_trk_el - dElement_eta;
							 float dphi_sec_tmp = TMath::Abs(phi_trk_el - dElement_phi);
                                                      if(dphi_sec_tmp>TMath::Pi())
                                                        {

                                                                dphi_sec_tmp = TMath::TwoPi() - dphi_sec_tmp;

                                                        }

                                                     float dphi =  dphi_sec_tmp;
                                                      hgtd_hits_deta_sec->Fill(dEta);
                                                      hgtd_hits_dphi_sec->Fill(dphi);

                                                      float dr = sqrt(dEta*dEta + dphi*dphi);
                                                      hgtd_hits_dR_sec->Fill(dr);
                                                    }// Particle Link check   
                                                   }// Secondaries Particles
          							
						// Deltas Particles
						if (barcode == 0)
                                                   {
                                                   if(particleLink.isValid()!=0)
                                                    {
                                                     
                                                    }// Particle Link check 
							 float dEta = eta_trk_el - dElement_eta;
                                                         float dphi_deltas_tmp = TMath::Abs(phi_trk_el - dElement_phi);
                                                      if(dphi_deltas_tmp>TMath::Pi())
                                                        {

                                                                dphi_deltas_tmp = TMath::TwoPi() - dphi_deltas_tmp;

                                                        }

                                                     float dphi =  dphi_deltas_tmp;
                                                      hgtd_hits_deta_deltas->Fill(dEta);
                                                      hgtd_hits_dphi_deltas->Fill(dphi);

                                                      float dr = sqrt(dEta*dEta + dphi*dphi);
                                                      hgtd_hits_dR_deltas->Fill(dr);	
		  
                                                 }// Delta particles
 			
					      }//deposit loop
					   
					 }//RDO loop					

                                }// cluster loop
                        }// m_ckuster loop

 

				ATH_MSG_DEBUG("      ------> Looked at a surface with BEC " <<BEC<<" layer "<<layer <<" eta "<<eta <<", centered on  = ("<<x<<", "<<y<<", "<<z<<")");
				delete *theSurf;
			}
			aux_extrapSurfaceCandidates.clear();


			for (auto coll : *m_clusters){
				for (auto cl: *coll){
					auto ID = cl->identify();
					int BEC = m_pixelID->barrel_ec(ID);
					int layer = m_pixelID->layer_disk(ID);
					int eta = m_pixelID->eta_module(ID);
					double x = cl->globalPosition().x();
					double y = cl->globalPosition().y();
					double z = cl->globalPosition().z();
					
					ATH_MSG_DEBUG("    There is a CLUSTER with BEC " <<BEC<<" layer "<<layer <<" eta "<<eta <<", loc = ("<<x<<", "<<y<<", "<<z<<"). Track direction is eta "<<track->eta()<<", phi "<<track->phi());
				}
			}

		}// Here are the extra-polated tracks I think so ////// Shahzad

		// clean up
		for (size_t k = 0; k < 4; ++k) {
			if (aux_extensionOutcome.at(k)) {
				delete aux_extensionOutcome.at(k);
				aux_extensionOutcome.at(k) = nullptr;
			}
		}
	}  // end of loop over tracks
	ATH_MSG_DEBUG("Done with this event ");

	// Filling number of Tracks Histogram
	hgtd_ntrk->Fill(ntrk);
	if(ntrk>0)
	{
		// First Layer
		hgtd_cluster_perEvent->Fill(n_cluster_perEvent); //
		hgtd_pixel_perEvent->Fill(n_pixel_perEvent);//
		hgtd_deposits_perEvent->Fill(n_depoist_perEvent);//
	}
        	// All Layers
        	//cout<<"Nmatch_clus = "<<Nmatch_clus<<"	Nmatch_pixel = "<<Nmatch_pixel<<"\tNmatch_deposit = "<<Nmatch_deposit<<endl; 
		hgtd_match_cluster_n->Fill(Nmatch_clus);
		hgtd_match_pixels_n->Fill(Nmatch_pixel);
		hgtd_match_deposits_n->Fill(Nmatch_deposit);

	

	return StatusCode::SUCCESS;
}

double Trk::TrackTimingExtensionAlg::getStraightLineToBeamSpot(const Trk::TrackParameters& params) {
	// why is this returning a ptr? do I take posession?
	auto globalPos = params.associatedSurface().localToGlobal(params.localPosition());
	auto beamSpotPos = m_beamSpotSvc->beamPos();
	double distance = (*globalPos - beamSpotPos).norm();
	// assuming it is my job to clean up
	delete globalPos;
	return distance;
}

double Trk::TrackTimingExtensionAlg::getTimeFromBeamSpot(const Trk::TrackParameters& params) {
	return getStraightLineToBeamSpot(params) / Gaudi::Units::c_light;
}
double Trk::TrackTimingExtensionAlg::getClusterTime(const InDet::PixelCluster* clus) const {
	// noemi is using omega x for this at the moment.
	// TODO: Not sure if we need a unit conversion??
	return clus->omegax();
}

StatusCode Trk::TrackTimingExtensionAlg::decorateExtensionOutcome(const xAOD::TrackParticle* tp,
		const std::array<const Trk::TrackStateOnSurface*, 4>& outcome,
		const double& R_L0_extrap,
		trackSelectionOutcome selectionOutcome) {
	// first, collect information
	double meanTime = 0;
	double meanTimeChi2 = 0;
	double meanTimeDenom = 0;
	double RMSTime = 0;
	int nLayers = 0;
	double trueTime = -1000;
	static SG::AuxElement::Accessor<ElementLink<xAOD::TruthParticleContainer>> acc_tpl("truthParticleLink");
	const xAOD::TruthParticle* TruthParticle = nullptr; 

	bool analyseIt = (selectionOutcome != trackSelectionOutcome::outOfAcceptance); 


	if (analyseIt){
		if (acc_tpl.isAvailable(*tp)) {
			auto truthMatchLink = acc_tpl(*tp);
			if (truthMatchLink.isValid()) {
				TruthParticle = *truthMatchLink;
				auto PV = TruthParticle->prodVtx();
				if (PV){
					// time is given in mm, because what the ...hug... 
					trueTime = PV->t() / Gaudi::Units::c_light;
				}
			}
		}
		if (!TruthParticle){
			ATH_MSG_WARNING("No truth match for a track - this may lead to problems with the truth decorations");
		}
	}


	std::vector<bool> hasCluster;
	std::vector<float> clusterChi2;
	std::vector<float> clusterR;
	std::vector<float> clusterTime;
	std::vector<float> clusterT0;
	std::vector<float> clusterDeltaT;
	std::vector<float> clusterX;
	std::vector<float> clusterY;
	std::vector<float> clusterZ;
	std::vector<int> clusterClassification;
	std::vector<bool> expectCluster;
	std::vector<bool> clusterMerged;
	std::vector<bool> clusterShadowing;
	std::vector<ElementLink<InDet::PixelClusterContainer > > clusterLink;


	std::array<bool,4> matchedClusters{false,false,false,false};

	if (analyseIt && TruthParticle){
		matchedClusters = getTruthMatchedClusters(TruthParticle);
	}
	int nexp = 0; 

	for (size_t ilayer = 0; ilayer < outcome.size(); ++ilayer) {
             cout<<"as a test ilayer = "<<ilayer<<endl;

		expectCluster.push_back(matchedClusters.at(ilayer)); 
		if (expectCluster[ilayer]) ++nexp;

		auto TSOS = outcome[ilayer];
		if (!TSOS) {
			hasCluster.push_back(false);
			clusterChi2.push_back(-1);
			clusterR.push_back(-1);
			clusterTime.push_back(-1);
			clusterT0.push_back(-1);
			clusterDeltaT.push_back(-1);
			clusterX.push_back(-1);
			clusterY.push_back(-1);
			clusterZ.push_back(-1);
			clusterClassification.push_back(-1);
			clusterMerged.push_back(false);
			clusterShadowing.push_back(false);
			// clusterLink.push_back(ElementLink<InDet::PixelClusterContainer>(nullptr,*clusters,evtStore()));


		} else {
			const InDet::PixelClusterOnTrack* onTrack = dynamic_cast<const InDet::PixelClusterOnTrack*>(TSOS->measurementOnTrack());
			hasCluster.push_back(true);
			double chi2 = TSOS->fitQualityOnSurface()->chiSquared() / TSOS->fitQualityOnSurface()->doubleNumberDoF(); 


			clusterChi2.push_back(chi2);
			clusterR.push_back(onTrack->globalPosition().perp());

			double t_raw = getClusterTime(onTrack->prepRawData());
			auto globalHitPos = TSOS->measurementOnTrack()->globalPosition();
			double z = globalHitPos.z() - tp->z0();
			// estimate t0 using a straight line approximation
			double t0 = sqrt(z * z + globalHitPos.perp() * globalHitPos.perp()) / Gaudi::Units::c_light;
			double dT = t_raw - t0;
                        
			h1->Fill(t0);// Test Histogram to see the t0
			
			clusterTime.push_back(t_raw);
			clusterT0.push_back(t0);
			clusterDeltaT.push_back(dT);

			clusterX.push_back(onTrack->globalPosition().x());
			clusterY.push_back(onTrack->globalPosition().y());
			clusterZ.push_back(onTrack->globalPosition().z());
			//    std::cout<<"clusterX = "<<onTrack->globalPosition().x()<<"\t clusterY = "<<onTrack->globalPosition().y()<<"\tclusterZ = "<<onTrack->globalPosition().z()<<std::endl;
			// Shahzad Added
			/*  TVector3 v1;
			    v1.SetXYZ(onTrack->globalPosition().x(), onTrack->globalPosition().y(), onTrack->globalPosition().z());
			    Double_t    m_eta= v1.PseudoRapidity();
			    Double_t   m_phi= v1.Phi();

			    hgtd_eta->Fill(m_eta);
			    hgtd_phi->Fill(m_phi);
			    hgtd_x->Fill(onTrack->globalPosition().x());
			    hgtd_y->Fill(onTrack->globalPosition().y());
			    hgtd_z->Fill(onTrack->globalPosition().z());
			    hgtd_r->Fill(sqrt(onTrack->globalPosition().x()*onTrack->globalPosition().x() + onTrack->globalPosition().y()*onTrack->globalPosition().y()));
			    hgtd_xy->Fill(onTrack->globalPosition().x(), onTrack->globalPosition().y());
			    m_cluster = m_cluster+1; */
			bool isMerged = true; 
			bool isShadowing = true; 
			auto classification = TruthParticle ? classifyCluster(dynamic_cast<const InDet::PixelCluster*>(onTrack->prepRawData()), TruthParticle, isShadowing, isMerged) : clusterTruthClassification::unmatched;

			clusterClassification.push_back(classification); 
			clusterMerged.push_back(isMerged); 
			clusterShadowing.push_back(isShadowing);

			meanTime += dT;
			meanTimeChi2 += dT / chi2;
			meanTimeDenom += 1./chi2;
			RMSTime += dT * dT;
			++nLayers;
		}
	}
	if (nLayers > 0) {
		meanTime = meanTime / (double)nLayers;
		meanTimeChi2 = meanTimeChi2 / meanTimeDenom;
		RMSTime = sqrt(RMSTime / (double)nLayers - meanTime * meanTime);
	}

	// now we are ready to decorate!
	m_dec_extensionCandidate->set(*tp, (selectionOutcome > trackSelectionOutcome::outOfAcceptance));
	m_dec_trackCategory->set(*tp, selectionOutcome);
	m_dec_trueProdTime->set(*tp, trueTime);
	m_dec_hasValidExtension->set(*tp, (nLayers >= m_nClus_min_ext));
	m_dec_nHGTDLayers->set(*tp, (nLayers));
	m_dec_nPossibleHGTDLayers->set(*tp, nexp);
	m_dec_nMissedHGTDLayers->set(*tp, nexp - nLayers);
	m_dec_meanTime->set(*tp, meanTime);
	m_dec_meanTimeChi2Weighted->set(*tp, meanTimeChi2);
	m_dec_rmsTime->set(*tp, RMSTime);
	m_dec_RonLayer0->set(*tp, R_L0_extrap);
	m_dec_perLayer_hasCluster->set(*tp, hasCluster);
	m_dec_perLayer_clusterChi2->set(*tp, clusterChi2);
	m_dec_perLayer_clusterR->set(*tp, clusterR);
	m_dec_perLayer_clusterRawTime->set(*tp, clusterTime);
	m_dec_perLayer_clusterDeltaT->set(*tp, clusterDeltaT);
	m_dec_perLayer_clusterT0->set(*tp, clusterT0);
	m_dec_perLayer_clusterX->set(*tp, clusterX);
	m_dec_perLayer_clusterY->set(*tp, clusterY);
	m_dec_perLayer_clusterZ->set(*tp, clusterZ);
	m_dec_perLayer_clusterTruthClassification->set(*tp, clusterClassification);
	m_dec_perLayer_clusterIsMerged->set(*tp, clusterMerged);
	m_dec_perLayer_clusterIsShadowing->set(*tp, clusterShadowing);
	m_dec_perLayer_expectCluster->set(*tp, expectCluster);
	// m_dec_perLayer_ClusterLink->set(*tp, clusterLink);
	//   std::cout<<" NHit = "<<m_cluster<<endl;
	//   hgtd_nclusters->Fill(m_cluster);
	return StatusCode::SUCCESS;
}
// Filling nCluster Hit
//hgtd_nclusters->Fill(m_cluster);
//std::cout<<" NHit = "<<m_cluster<<endl;
// ------
Trk::TrackTimingExtensionAlg::clusterTruthClassification Trk::TrackTimingExtensionAlg::classifyCluster(const InDet::PixelCluster* clus, const xAOD::TruthParticle* truthMatch, bool & isShadowingTrack, bool & isMerged){

	auto RDOList = clus->rdoList();
	std::vector<std::pair< clusterTruthClassification, bool> > RDOoutcomes;
	// pixels in cluster
	for (auto rdoID : RDOList){
		// get the time of flight to the detector element 
		auto detectorElement = m_pixelManager->getDetectorElement(rdoID);
		double ToF_prod = detectorElement->center().mag() / Gaudi::Units::c_light;
		// now look for the RDO in the SDO map 
		auto pos = m_sdoCollection->find(rdoID);
		// deposits in pixel
		if (pos == m_sdoCollection->end()){
			ATH_MSG_WARNING("RDO not found in SDO map");
		}
		// collect deposits
		std::map <float, clusterTruthClassification> mydeposits;
		clusterTruthClassification thisType;

		for(auto deposit : pos->second.getdeposits() ){
			const HepMcParticleLink& particleLink = deposit.first;
			int barcode = particleLink.barcode();

			// Noemi uses a +/- 1ns time window for clusterisation - so make sure to also apply this here
			if (fabs(deposit.second - ToF_prod) > 1.0) continue; 
			if (barcode == 0 || barcode > 200000){
				mydeposits.emplace (deposit.second - ToF_prod, clusterTruthClassification::matchedToSecondary);
			}
			// check for identity with original particle 
			const HepMC::GenParticle *part=particleLink.cptr();
			if (part){
				TLorentzVector l4 (part->momentum().px(),part->momentum().py(),part->momentum().pz(),part->momentum().e());
  				//-**************************/////
				h2->Fill(truthMatch->p4().DeltaR(l4));
                                //--*************************/////////
				if (barcode == truthMatch->barcode() && truthMatch->p4().DeltaR(l4) < 0.05){
					mydeposits.emplace (deposit.second - ToF_prod, clusterTruthClassification::matchedToTrack);
				}
				else if (part->parent_event() == m_hardScatterEvent){                      
					mydeposits.emplace (deposit.second - ToF_prod, clusterTruthClassification::matchedToHS);
				}
				else {
					mydeposits.emplace (deposit.second - ToF_prod, clusterTruthClassification::matchedToPileUp);
				}
			}
			else {
				ATH_MSG_WARNING("Careful, a deposit with BC"<<barcode<<" has no truth match - guessing based on event index...");
				if (particleLink.eventIndex() == 0){
					mydeposits.emplace (deposit.second - ToF_prod, clusterTruthClassification::matchedToHS);
				}
				else {
					mydeposits.emplace (deposit.second - ToF_prod, clusterTruthClassification::matchedToPileUp);
				}
			}
		}
		// check for shadowing
		bool isShadowing = false; 
		for (std::map<float, clusterTruthClassification>::iterator elem = mydeposits.begin(); elem != mydeposits.end(); ++elem){
			if (elem == mydeposits.begin()){
				thisType = elem->second;
			}
			else {
				if (thisType != clusterTruthClassification::matchedToTrack && elem->second == clusterTruthClassification::matchedToTrack){
					isShadowing = true;
				}
			}
		}
		RDOoutcomes.emplace_back(thisType, isShadowing);
	}// Pixel in cluster loop
	if (RDOoutcomes.size() == 0){
		ATH_MSG_WARNING("did not manage to understand any RDOs...");
		isShadowingTrack = false; 
		isMerged = false; 
		return clusterTruthClassification::unmatched;
	}
	else {
		isMerged = false; 
		clusterTruthClassification classi = RDOoutcomes.at(0).first; 
		isShadowingTrack = RDOoutcomes.at(0).second;
		for (size_t k = 1; k < RDOoutcomes.size(); ++k){
			if (RDOoutcomes.at(k).first != classi){
				isMerged = true; 
			}
			else{
				isShadowingTrack |= RDOoutcomes.at(k).second;
			}

		}
		ATH_MSG_DEBUG("Finished classifying a cluster - classification "<<classi<<", is shadowing track? "<<isShadowingTrack<<", is merged? "<<isMerged);
		return classi;
	}
}

std::array<bool,4> Trk::TrackTimingExtensionAlg::getTruthMatchedClusters(const xAOD::TruthParticle* truth){

	std::array<bool,4>  outcome {false,false,false,false}; 
	// find where we need to look 
	const TrackParameters* truthAtPerigee = getTruthAtPerigee(truth); 
	if (!truthAtPerigee){
		ATH_MSG_INFO("Skipping cluster collection due to invalid perigee extrap (likely no prod vtx");
		return outcome;
	} 
	if (fabs(truth->eta()) < 2){
		ATH_MSG_INFO("Skipping an absurdly central truth match");
		return outcome;
	}
	// now go through the layers 
	bool aSide = (truth->pz() > 0);
	bool shadow=false;
	bool merged=false;
	for (size_t layer = 0; layer < 4; ++layer){
		bool foundOne = false;
		const Trk::PlaneLayer* theLayer = (aSide ? m_layers_A[layer] : m_layers_C[layer]);
		// look for close-by surface. Use a wide window since we extrapolate from the perigee. 
		auto surfaces = getCompatibleSurfaces(theLayer, truthAtPerigee, 5);
		// now we look at the clusters on our surfaces and find the ones matching our truth particle
		std::vector<Identifier> ids;
		std::for_each(surfaces.begin(), surfaces.end(), [&](const Trk::Surface* surf){ids.push_back(surf->associatedDetectorElementIdentifier());});
		for (auto collection : *m_clusters) {
			if (foundOne) break;
			if (std::find (ids.begin(),ids.end(), collection->identify()) != ids.end()){
				for (auto clus : *collection) {
					if (classifyCluster(clus, truth, shadow,merged) == clusterTruthClassification::matchedToTrack){
						foundOne = true;
						break;
					}
				}
			}
		}
		outcome[layer] = foundOne;
	}
	delete truthAtPerigee;
	return outcome;
}

StatusCode Trk::TrackTimingExtensionAlg::getHardScatterEvent(){

	const DataHandle<McEventCollection> mcCollptr;
	ATH_CHECK(evtStore()->retrieve(mcCollptr, "TruthEvent"));
	if (mcCollptr->size()==0){
		ATH_MSG_ERROR("Failed to retrieve a nonzero sized truth event collection");
		return StatusCode::FAILURE;
	}
	m_hardScatterEvent = mcCollptr->at(0); 
	return StatusCode::SUCCESS;
}
const Trk::TrackParameters* Trk::TrackTimingExtensionAlg::getTruthAtPerigee(const xAOD::TruthParticle* truth){
	if (!truth){
		ATH_MSG_WARNING("Can not get perigee for a nullptr truth");
		return nullptr;
	}
	const Amg::Vector3D momentum(truth->px(), truth->py(), truth->pz());
	const int pid(truth->pdgId());
	double charge = truth->charge();
	if (std::isnan(charge)) {
		ATH_MSG_WARNING("charge not found on truth with pid " << pid);
		return nullptr;
	}

	const xAOD::TruthVertex* ptruthVertex(0);
	try {
		ptruthVertex = truth->prodVtx();
	} catch (std::exception e) {
		return nullptr;
		ATH_MSG_WARNING("Failed to retrieve prod vtx");
	}
	if (!ptruthVertex) {
		ATH_MSG_WARNING("A production vertex pointer was retrieved, but it is NULL");
		return nullptr;
	}
	const auto xPos = ptruthVertex->x();
	const auto yPos = ptruthVertex->y();
	const auto z_truth = ptruthVertex->z();
	const Amg::Vector3D position(xPos, yPos, z_truth);
	// delete ptruthVertex;ptruthVertex=0;
	const Trk::CurvilinearParameters cParameters(position, momentum, charge);
	Trk::PerigeeSurface persf(m_beamSpotSvc->beamPos());

	return m_extrapolator->extrapolate(cParameters, persf, Trk::anyDirection, false);
}
