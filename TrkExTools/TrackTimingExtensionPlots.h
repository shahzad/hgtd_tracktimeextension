/*
Algorithm to plot the HGTD timing information check outcome
*/

#ifndef TRKEXTOOLS_TRACKTIMINGEXTENSIONPLOTS__H
#define TRKEXTOOLS_TRACKTIMINGEXTENSIONPLOTS__H

#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ToolHandle.h"
#include "TrkGeometry/PlaneLayer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "GaudiKernel/ITHistSvc.h"
#include <memory> 

#include "TEfficiency.h"
#include "TH2D.h"
#include "TH1D.h"
#include "TProfile.h"

namespace Trk {

  class TrackTimingExtensionPlots : public AthAlgorithm {
     public:
      TrackTimingExtensionPlots(const std::string& name, ISvcLocator* pSvcLocator);
      ~TrackTimingExtensionPlots();
  
      StatusCode initialize();
      StatusCode execute();
      StatusCode finalize();
  
     private:
     
     StatusCode PlotIt (const xAOD::TrackParticle* tp);

      std::string m_deco_name;
      std::string m_TrackParticleContainer;

      std::unique_ptr<SG::AuxElement::Accessor<char > > m_dec_extensionCandidate;
      std::unique_ptr<SG::AuxElement::Accessor<char > > m_dec_hasValidExtension;
      std::unique_ptr<SG::AuxElement::Accessor<int > > m_dec_nHGTDLayers;
      std::unique_ptr<SG::AuxElement::Accessor<float > > m_dec_trueProdTime;
      std::unique_ptr<SG::AuxElement::Accessor<float > > m_dec_meanTime;
      std::unique_ptr<SG::AuxElement::Accessor<float > > m_dec_meanTimeChi2Weighted;
      std::unique_ptr<SG::AuxElement::Accessor<float > > m_dec_rmsTime;
      std::unique_ptr<SG::AuxElement::Accessor<float > > m_dec_RonLayer0;
      std::unique_ptr<SG::AuxElement::Accessor<std::vector<bool> > > m_dec_perLayer_hasCluster;
      std::unique_ptr<SG::AuxElement::Accessor<std::vector<float> > > m_dec_perLayer_clusterChi2;
      std::unique_ptr<SG::AuxElement::Accessor<std::vector<float> > > m_dec_perLayer_clusterR;
      std::unique_ptr<SG::AuxElement::Accessor<std::vector<float> > > m_dec_perLayer_clusterRawTime;
      std::unique_ptr<SG::AuxElement::Accessor<std::vector<float> > > m_dec_perLayer_clusterT0;
      std::unique_ptr<SG::AuxElement::Accessor<std::vector<float> > > m_dec_perLayer_clusterDeltaT;
      std::unique_ptr<SG::AuxElement::Accessor<std::vector<float> > > m_dec_perLayer_clusterX;
      std::unique_ptr<SG::AuxElement::Accessor<std::vector<float> > > m_dec_perLayer_clusterY;
      std::unique_ptr<SG::AuxElement::Accessor<std::vector<float> > > m_dec_perLayer_clusterZ;

      ServiceHandle<ITHistSvc> m_histSvc; 

      TH1D* nHits_raw;
      TEfficiency* eff_eta_atLeastOneA;
      TEfficiency* eff_eta_atLeastTwoA;
      TEfficiency* eff_eta_atLeastThreeA;
      TEfficiency* eff_eta_atLeastOneC;
      TEfficiency* eff_eta_atLeastTwoC;
      TEfficiency* eff_eta_atLeastThreeC;
      TEfficiency* eff_phi_atLeastOne;
      TEfficiency* eff_phi_atLeastTwo;
      TEfficiency* eff_phi_atLeastThree;
      TEfficiency* eff_R_atLeastOne;
      TEfficiency* eff_R_atLeastTwo;
      TEfficiency* eff_R_atLeastThree;
      TEfficiency* eff_RPhi_atLeastOne;
      TH1D* clusterChiSquare_raw;
      TH1D* clusterRMS_twoHits;
      TH1D* clusterRMS_threeHits;
      TH1D* measTimeCluster;
      TH1D* measTimeTrackChi2;
      TH1D* measTimeTrackMean;
      TH2D* measTimeVertexVersusTruth;
      TH1D* measDeltaTimeVertexVersusTruth;
      //---------------------------------------Shahzad Added Lines---------------------
      

  };

}

#endif  // TRKEXTOOLS_TrackTimingExtensionPlots__H
