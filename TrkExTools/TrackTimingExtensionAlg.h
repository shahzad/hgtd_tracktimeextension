/*
   Algorithm to add HGTD timing information to a track
   collection, using the TrackTimingExtensionTool
   from this package
   */

#ifndef TRKEXTOOLS_TRACKTIMINGEXTENSIONALG__H
#define TRKEXTOOLS_TRACKTIMINGEXTENSIONALG__H

#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ToolHandle.h"
#include "TrkGeometry/PlaneLayer.h"
#include "InDetPrepRawData/PixelCluster.h"
#include "TrkExInterfaces/IExtrapolator.h"
#include "InDetPrepRawData/PixelClusterContainer.h"
#include "TrkToolInterfaces/IUpdator.h"
#include "TrkToolInterfaces/ITrackSelectorTool.h"
#include "InDetBeamSpotService/IBeamCondSvc.h"

#include "InDetIdentifier/PixelID.h"
#include "InDetReadoutGeometry/PixelDetectorManager.h"
#include "InDetIdentifier/SCT_ID.h"
#include "InDetReadoutGeometry/SCT_DetectorManager.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "InDetSimData/InDetSimDataCollection.h"
#include "GeneratorObjects/McEventCollection.h"
#include "GaudiKernel/ITHistSvc.h"
#include <memory> 
#include "TH2D.h"
#include "TH1D.h"
#include "TProfile.h"
#include "TTree.h"
class PixelID;
class SCT_ID;
class AtlasDetectorID;
class TH1;
class TH2;
class TTree;
namespace InDetDD {
	class PixelDetectorManager;
	class SCT_DetectorManager;
}

namespace Trk {

	class TrackTimingExtensionAlg : public AthAlgorithm {
		public:
			TrackTimingExtensionAlg(const std::string& name, ISvcLocator* pSvcLocator);
			~TrackTimingExtensionAlg();

			StatusCode initialize();
			StatusCode execute();
			StatusCode finalize();

			enum trackSelectionOutcome{
				outOfAcceptance=0,
				failHitCuts=1,
				failLastLayers=2,
				passAll=3
			};

			enum clusterTruthClassification{ 
				unmatched=0,
				matchedToTrack=1,
				matchedToHS=2,
				matchedToPileUp=3,
				matchedToSecondary=4 
			};

		private:
			// populate internal surface arrays from det store during init
			StatusCode populateSurfaceArrays();
			//ServiceHandle<ITHistSvc> m_histSvc;   // Histogram vairaible   
			// get the surfaces close to the extrapolated position of a track extropalated onto the layer. The nSteps parameter steers the window size in bins used for the lookup (larger = more surfaces)
			std::vector < const Trk::Surface* > getCompatibleSurfaces(const Trk::PlaneLayer * layer, const TrackParameters* tps, int nSteps = 5);

			const TrackParameters* getTruthAtPerigee(const xAOD::TruthParticle* truth);

			// get the starting point for our extrapolation
			const Trk::TrackStateOnSurface* getLastHit(const xAOD::TrackParticle* tp);

			// track selection                                                                                           
			trackSelectionOutcome decision(const Trk::TrackStateOnSurface* lastTSOS, const xAOD::TrackParticle* tp);

			// get the candidate surface for a given layer 
			// return outcome by populating the vector (passed by ref) with extrapolated TP for each surface 
			void findCandidateSurfaces(size_t layer, const Trk::TrackStateOnSurface* currentState, std::vector<const Trk::TrackParameters*> & candidates); 

			clusterTruthClassification classifyCluster(const InDet::PixelCluster* clus, const xAOD::TruthParticle* truthMatch, bool & isShadowingTrack, bool & isMerged);

			// get HGTD clusters for the surface associated to the passed TP 
			// for each, we will perform the forward extrap.
			// return a TSOS  for the progressive filter outcome with the 'best' outcome  for this surface 
			// this should internally check the quality of the clusters to consider for a matching! 
			const Trk::TrackStateOnSurface* bestAssocPixelCluster(const Trk::TrackParameters & trkPar);

			// perform update step for one cluster and an extrapolated track
			const Trk::TrackStateOnSurface* updateWithSurface(const Trk::TrackParameters & trkPar, const  InDet::PixelCluster* cluster); 

			// decorate the extension results to our track for analysis.
			StatusCode decorateExtensionOutcome(const xAOD::TrackParticle* tp, const std::array<const Trk::TrackStateOnSurface*,4>& outcome, const double & R_L0_extrap, trackSelectionOutcome selectionOutcome);

			std::array<bool,4> getTruthMatchedClusters(const xAOD::TruthParticle* tp);

			// get distance of position to beam spot in mm 
			double getStraightLineToBeamSpot(const Trk::TrackParameters & params);
			// get straight line t.o.f for v=c from beam spot to position
			double getTimeFromBeamSpot(const Trk::TrackParameters & params);
			// get the time info from a cluster (noemi's hack) in picoseconds
			double getClusterTime(const InDet::PixelCluster* clus) const; 
			// retrieve the hard scatter
			StatusCode getHardScatterEvent(); 
			// extrapolation tool
			ToolHandle<Trk::IExtrapolator> m_extrapolator;
			// beam spot service
			ServiceHandle<IBeamCondSvc> m_beamSpotSvc;
			// kalman updator
			ToolHandle<Trk::IUpdator> m_updator;
			// track selection, run before attempting to add timing
			ToolHandle<Trk::ITrackSelectorTool> m_trackSelector;
			// SG key of HGTD cluster container
			std::string m_HGTDClusterContainer;
			// SG key of track particle container - will be decorated in-situ
			std::string m_TrackParticleContainer;
			// name for decoration to be used when adding timing info
			std::string m_deco_name;

			//       Plane Layers - A and C side (pick which based on pz)
			std::vector<const Trk::PlaneLayer*> m_layers_A;
			std::vector<const Trk::PlaneLayer*> m_layers_C;

			// chi square cut for successful extension
			double m_cut_chi2_ext; 
			// min number of clusters for a successful extension
			int m_nClus_min_ext; 
			// timing window width to select cluster candidates for extension 
			// in picoseconds 
			double m_dT_clusterSelection;

			const PixelID *m_pixelID;
			const SCT_ID *m_stripsID;
			const InDetDD::PixelDetectorManager   *m_pixelManager;
			const InDetDD::SCT_DetectorManager    *m_stripsManager;

			// needed for truth studies
			const InDet::PixelClusterContainer* m_clusters;
			const InDetSimDataCollection* m_sdoCollection;
			const HepMC::GenEvent* m_hardScatterEvent; 

			// first HGTD layer is 5, hence needed offset for vectors and arrays
			int m_layerOffset;


			// define bins and extension for the BinUtilities
			int m_bins;
			float m_min; // mm
			float m_max; // mm
			int m_Ncluster; // number of cluster per Event 

			std::unique_ptr<SG::AuxElement::Decorator<int > > m_dec_trackCategory;
			std::unique_ptr<SG::AuxElement::Decorator<char > > m_dec_extensionCandidate;
			std::unique_ptr<SG::AuxElement::Decorator<char > > m_dec_hasValidExtension;
			std::unique_ptr<SG::AuxElement::Decorator<int > > m_dec_nHGTDLayers;
			std::unique_ptr<SG::AuxElement::Decorator<int > > m_dec_nPossibleHGTDLayers;
			std::unique_ptr<SG::AuxElement::Decorator<int > > m_dec_nMissedHGTDLayers;
			std::unique_ptr<SG::AuxElement::Decorator<float > > m_dec_trueProdTime;
			std::unique_ptr<SG::AuxElement::Decorator<float > > m_dec_meanTime;
			std::unique_ptr<SG::AuxElement::Decorator<float > > m_dec_meanTimeChi2Weighted;
			std::unique_ptr<SG::AuxElement::Decorator<float > > m_dec_expTimeFromZ0;
			std::unique_ptr<SG::AuxElement::Decorator<float > > m_dec_rmsTime;
			std::unique_ptr<SG::AuxElement::Decorator<float > > m_dec_RonLayer0;
			std::unique_ptr<SG::AuxElement::Decorator<std::vector<bool> > > m_dec_perLayer_hasCluster;      
			std::unique_ptr<SG::AuxElement::Decorator<std::vector<float> > > m_dec_perLayer_clusterChi2;
			std::unique_ptr<SG::AuxElement::Decorator<std::vector<float> > > m_dec_perLayer_clusterR;
			std::unique_ptr<SG::AuxElement::Decorator<std::vector<float> > > m_dec_perLayer_clusterRawTime;
			std::unique_ptr<SG::AuxElement::Decorator<std::vector<float> > > m_dec_perLayer_clusterT0;
			std::unique_ptr<SG::AuxElement::Decorator<std::vector<float> > > m_dec_perLayer_clusterDeltaT;
			std::unique_ptr<SG::AuxElement::Decorator<std::vector<float> > > m_dec_perLayer_clusterX;
			std::unique_ptr<SG::AuxElement::Decorator<std::vector<float> > > m_dec_perLayer_clusterY;
			std::unique_ptr<SG::AuxElement::Decorator<std::vector<float> > > m_dec_perLayer_clusterZ;
			std::unique_ptr<SG::AuxElement::Decorator<std::vector<int> > > m_dec_perLayer_clusterTruthClassification; 
			std::unique_ptr<SG::AuxElement::Decorator<std::vector<bool> > > m_dec_perLayer_clusterIsMerged; 
			std::unique_ptr<SG::AuxElement::Decorator<std::vector<bool> > > m_dec_perLayer_clusterIsShadowing; 
			std::unique_ptr<SG::AuxElement::Decorator<std::vector<bool> > > m_dec_perLayer_expectCluster; 
			// std::unique_ptr<SG::AuxElement::Decorator<std::vector<ElementLink<InDet::PixelClusterContainer > > > > m_dec_perLayer_ClusterLink; 
			ServiceHandle<ITHistSvc> m_histSvc;
			TH1D* hgtd_nclusters;
			TH1D* hgtd_npixles;
			TH1D* hgtd_npixles_prim;// Number of Primaries  Deposites in a Cluster
			TH1D* hgtd_npixles_sec;// Number of Secondaries Deposites in a Cluster
			// clusters
			TH1D* hgtd_x; 
			TH1D* hgtd_y;
			TH1D* hgtd_z;
			TH1D* hgtd_r;
			TH1D* hgtd_layer;
			TH2D* hgtd_xy;
			TH1D* hgtd_eta;
			TH1D* hgtd_phi;
			TH1D* hgtd_clus_time;
			TH1D* hgtd_det_element_time;
			// Deposits 
			TH1D* hgtd_x_avg;
			TH1D* hgtd_y_avg;
			TH1D* hgtd_dx_prim_sec;
			TH1D* hgtd_dy_prim_sec;
			TH1D* hgtd_dR_prim_sec;
			// Primary particles clusters
			TH1D* hgtd_x_prim;
			TH1D* hgtd_y_prim;
			TH1D* hgtd_z_prim;
			TH1D* hgtd_r_prim;
			TH1D* hgtd_layer_prim;
			TH2D* hgtd_xy_prim;
			TH1D* hgtd_eta_prim;
			TH1D* hgtd_phi_prim;
			TH1D* hgtd_clus_time_prim;  
			TH1D* hgtd_deltaR_prim;
			TH1D* hgtd_deltaR_prim_L1;
			TH1D* deposit_prim_time;      
			TH1D* hgtd_time_prim;
			TH1D* hgtd_time_window_prim;    
			// Production vertices of particles

			TH1D* hgtd_vtx_prim;
			TH1D* hgtd_vty_prim;
			TH1D* hgtd_vtz_prim;
			TH1D* hgtd_vtr_prim;
			TH2D* hgtd_vtrz_prim;

			// secondaries particles clusters
			TH1D* hgtd_x_sec;
			TH1D* hgtd_y_sec;
			TH1D* hgtd_z_sec;
			TH1D* hgtd_r_sec;

			TH1D* hgtd_time_sec;
			TH1D* hgtd_time_window_sec; 
			TH1D* hgtd_layer_sec;
			TH2D* hgtd_xy_sec;
			TH1D* hgtd_eta_sec;
			TH1D* hgtd_phi_sec;
			TH1D* hgtd_deltaR_sec;
			TH1D* hgtd_deltaR_sec_L1;

			TH1D* h1;/// Test Histogram
			TH1D* h2;// Delta l4 test

			TH1D* hgtd_x_avg_sec;
			TH1D* hgtd_y_avg_sec;
			TH1D* hgtd_dx_avg_sec;
			TH1D* hgtd_dy_avg_sec;
			TH1D* hgtd_dR_avg_sec;

			TH1D* hgtd_eta_avg_sec;
			TH1D* hgtd_phi_avg_sec;
			TH1D* hgtd_deta_avg_sec;
			TH1D* hgtd_dphi_avg_sec;
			TH1D* hgtd_delta_R_avg_sec;

			TH1D* hgtd_eta_avg_sec_L1;
			TH1D* hgtd_phi_avg_sec_L1;
			TH1D* hgtd_deta_avg_sec_L1;
			TH1D* hgtd_dphi_avg_sec_L1;
			TH1D* hgtd_delta_R_avg_sec_L1;

			TH1D* hgtd_x_avg_sec_L1;
			TH1D* hgtd_y_avg_sec_L1;
			TH1D* hgtd_dx_avg_sec_L1;
			TH1D* hgtd_dy_avg_sec_L1;
			TH1D* hgtd_dR_avg_sec_L1;

			// production vertices for secondaries particles
			TH1D* hgtd_vtx_sec;
			TH1D* hgtd_vty_sec;
			TH1D* hgtd_vtz_sec;
			TH1D* hgtd_vtr_sec;
			TH2D* hgtd_vtrz_sec;//
			// Delta Particle
			TH1D* hgtd_x_deltas;
			TH1D* hgtd_y_deltas;
			TH1D* hgtd_z_deltas;
			TH1D* hgtd_r_deltas;
			TH1D* hgtd_layer_deltas;
			TH2D* hgtd_xy_deltas;
			TH1D* hgtd_eta_deltas;
			TH1D* hgtd_phi_deltas;
			//TH1D* hgtd_deltaR_deltas;
			TH1D* hgtd_clus_time_deltas;
			TH2D* dR_vs_TOF_deltas;
			TH1D* deposit_deltas_time;

			TH1D* hgtd_time_deltas;
			TH1D* hgtd_time_window_deltas;

			TH1D* hgtd_deltaR_deltas;
			TH1D* hgtd_deltaR_deltas_L1;

			TH1D* hgtd_x_avg_deltas;
			TH1D* hgtd_y_avg_deltas;
			TH1D* hgtd_dx_avg_deltas;
			TH1D* hgtd_dy_avg_deltas;
			TH1D* hgtd_dR_avg_deltas;

			TH1D* hgtd_x_avg_deltas_L1;
			TH1D* hgtd_y_avg_deltas_L1;
			TH1D* hgtd_dx_avg_deltas_L1;
			TH1D* hgtd_dy_avg_deltas_L1;
			TH1D* hgtd_dR_avg_deltas_L1;

			TH1D* hgtd_eta_avg_deltas;
			TH1D* hgtd_phi_avg_deltas;
			TH1D* hgtd_deta_avg_deltas;
			TH1D* hgtd_dphi_avg_deltas;
			TH1D* hgtd_delta_R_avg_deltas;

			TH1D* hgtd_eta_avg_deltas_L1;
			TH1D* hgtd_phi_avg_deltas_L1;
			TH1D* hgtd_deta_avg_deltas_L1;
			TH1D* hgtd_dphi_avg_deltas_L1;
			TH1D* hgtd_delta_R_avg_deltas_L1;   
			// production vertices for Deltas particles
			TH1D* hgtd_vtx_deltas;
			TH1D* hgtd_vty_deltas;
			TH1D* hgtd_vtz_deltas;
			TH1D* hgtd_vtr_deltas;
			TH2D* hgtd_vtrz_deltas;//

			// Track Histogram
			TH1D* hgtd_ntrk;
			TH1D* hgtd_trk_pt;
			TH1D* hgtd_trk_eta;
			TH1D* hgtd_trk_phi;
			// Production vertices info for primaries and secondaries
			TH1D* prod_vertex_bcod_prim;
			TH1D* prod_vertex_bcod_sec;
			Int_t n_event;
			Int_t ievt;

			// Histogram for claculating number of clusters, Pixels and deposites in an event 
			TH1D* hgtd_cluster_perEvent;
			TH1D* hgtd_pixel_perEvent;  
			TH1D* hgtd_deposits_perEvent;
			TH1D* hgtd_evt_inclusive;
			//TH1D* hgtd_deposits_perEvent;
			TH1D* hgtd_deposits_perEvent_prim;
			TH1D* hgtd_deposits_perEvent_sec;
			TH1D* hgtd_deposits_perEvent_deltas;

			//------------------Pixel Size--------

			TH1D* hgtd_loc_x_Pixel;
			TH1D* hgtd_loc_y_Pixel;
			TH1D* hgtd_loc_z_Pixel;	
			TH2D* hgtd_loc_xy_Pixel;

			TH1D* hgtd_loc_x_Pixel_L1;
                        TH1D* hgtd_loc_y_Pixel_L1;
                        TH1D* hgtd_loc_z_Pixel_L1;
                        TH2D* hgtd_loc_xy_Pixel_L1;
			
			// Track and Hits histos
                        // Primaries 
			TH1D* hgtd_hits_eta_prim;
			TH1D* hgtd_hits_phi_prim;

                        TH1D* hgtd_hits_deta_prim;
                        TH1D* hgtd_hits_dphi_prim;
          
     			TH1D* hgtd_hits_dR_prim;
                        // Secondaries 

 			TH1D* hgtd_hits_eta_sec;
                        TH1D* hgtd_hits_phi_sec;

                        TH1D* hgtd_hits_deta_sec;
                        TH1D* hgtd_hits_dphi_sec;

                        TH1D* hgtd_hits_dR_sec;
                        // Deltas
			TH1D* hgtd_hits_eta_deltas;
                        TH1D* hgtd_hits_phi_deltas;

                        TH1D* hgtd_hits_deta_deltas;
                        TH1D* hgtd_hits_dphi_deltas;

                        TH1D* hgtd_hits_dR_deltas;

			TH1D* hgtd_match_cluster_n;
			TH1D* hgtd_match_pixels_n;
			TH1D* hgtd_match_deposits_n;
			// Ntuples

			std::vector<float>* m_nHit_prim = nullptr;
			std::vector<float>* m_nHit_sec = nullptr;// secondary hits
			std::vector<float>* m_nHit_pLink_sec = nullptr;// store those prticle for which the paticle is valide>0
			//-------------- Distance Particles Ntuples

			std::vector<float>* m_avg_x_deposits =  nullptr;
			std::vector<float>* m_avg_x_prim =  nullptr;
			std::vector<float>* m_avg_x_sec =  nullptr;
			std::vector<float>* m_avg_x_deltas =  nullptr;

			std::vector<float>* m_avg_y_deposits =  nullptr;
			std::vector<float>* m_avg_y_prim =  nullptr;
			std::vector<float>* m_avg_y_sec =  nullptr;
			std::vector<float>* m_avg_y_deltas =  nullptr;

			std::vector<float>* m_dx_deposits =  nullptr;
			std::vector<float>* m_dx_prim =  nullptr;
			std::vector<float>* m_dx_sec =  nullptr;
			std::vector<float>* m_dx_deltas =  nullptr;

			std::vector<float>* m_dy_deposits =  nullptr;
			std::vector<float>* m_dy_prim =  nullptr;
			std::vector<float>* m_dy_sec =  nullptr;
			std::vector<float>* m_dy_deltas =  nullptr;

			std::vector<float>* m_dR_deposits =  nullptr;
			std::vector<float>* m_dR_prim =  nullptr;
			std::vector<float>* m_dR_sec =  nullptr;
			std::vector<float>* m_dR_deltas =  nullptr;

			std::vector<float>* m_nHit_deltas = nullptr;
			std::vector<float>* m_detector_ele_tim = nullptr; 
			std::vector<float>* m_deposit_time = nullptr;// I think it is the deposit time
			std::vector<float>* m_deposit_prim_time = nullptr;
			std::vector<float>* m_deposit_sec_time = nullptr;
			std::vector<float>* m_ndeposit = nullptr;// Number hits in a cluster     
			std::vector<float>* m_pixel_x = nullptr;// pixel x

			std::vector<float>* m_cluster_perEvent = nullptr; // number of cluster per event
			std::vector<float>* m_pixel_perEvent = nullptr; // number of pixel per event 
			std::vector<float>* m_deposits_perEvent = nullptr; // number of deposites per event
			std::vector<float>* m_deposits_perEvent_prim = nullptr; // number of prim deposites per event
			std::vector<float>* m_deposits_perEvent_sec = nullptr; // number of Sec deposites per event    
			std::vector<float>* m_deposits_perEvent_deltas = nullptr; // number of Deltas deposites per event   

			std::vector<float>* m_collection_cluster = nullptr; // collection

			TTree* m_tree;
			std::string m_ntupleFileName; 
			void tree_clear(void);
	};

}

#endif  // TRKEXTOOLS_TRACKTIMINGEXTENSIONALG__H
